import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FavouriteDevicesComponent } from './favourite-devices.component';


describe('MaxMovieNightComponent', () => {
  let component: FavouriteDevicesComponent;
  let fixture: ComponentFixture<FavouriteDevicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FavouriteDevicesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouriteDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
