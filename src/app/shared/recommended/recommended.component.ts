import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { Movie } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { APIS } from 'src/app/apis';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'kismat-recommended',
  templateUrl: './recommended.component.html',
  styleUrls: ['./recommended.component.scss']
})
export class RecommendedComponent implements OnInit {
  @Input() videoId: string = '';
  @Input() videoType: string = '';
  @Input() title: string = "Kismet Specials";
  
  @Input() noPadding: boolean = false;
  @Input() noPaddingTop: boolean = false;
  @Input() noPaddingBottom: boolean = false;

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public commonservice: CommonService, public modalservice: ModalService,
    public videocontrolservice: VideoControlsService, public redirectservice: RedirectService) {
  }

  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 5,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 4.2,
      },
      992: {
        slidesPerView: 5.2,
      },
      1400: {
        slidesPerView: 7.2,
      }
    }
  };

  videos: any;

  ngOnInit(): void {
    this.getVideos();
  }

  getVideos() {
    this.commonservice.getMethod(APIS.ADMIN_GET_RECOMMENDED_VIDEOS(this.videoId, this.videoType))
    .subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.videos = res.data
        }
      });
  }
}
