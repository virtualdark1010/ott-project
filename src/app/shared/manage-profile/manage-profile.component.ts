import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-manage-profile',
  templateUrl: './manage-profile.component.html',
  styleUrls: ['./manage-profile.component.scss']
})
export class ManageProfileComponent implements OnInit {
  userInfo: any;
  profile_pic: string = "assets/imgs/icons/user.png";

  constructor(private commonservice: CommonService, public http: HttpClient) { }

  ngOnInit(): void {
    this.userInfo = this.commonservice.getuserInfo();
    console.log("userInfo", this.userInfo);

    if(this.userInfo.userDP){
      this.profile_pic = this.userInfo.userDP;
    }
  }

  changeProfile(event: any) {
    if (event.files.length == 0) {
      this.commonservice.snackbar('Please select a pic', 'warning');
      return
    }

    const formData = new FormData();
    formData.append("multipartFile", event.files[0]);
    let url = APIS.UPLOAD_PROFILE_PIC(this.userInfo.userId);

    this.http.post(url, formData).subscribe((res: any) => {
      console.log("res", res);
      if (res.statusCode == 200) {
        this.commonservice.snackbar(res.message, 'success');
        let _userInfo = {
          ...this.userInfo,
          userDP: res.data
        }
        this.commonservice.setuserInfo(_userInfo);
        this.ngOnInit();
        this.commonservice.changeProfile(true);
      } else {
        this.commonservice.snackbar(res.message, 'error');
      }
    })
  }

}
