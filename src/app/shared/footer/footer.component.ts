import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }
  linkItems = [
        { label: 'Privacy Policy', link: '/privacy' },
        { label: 'Terms of Use', link: '/termsofuse' },
        { label: 'Contact Us', link: '/contactus' },
  ]

  ngOnInit(): void {
  }

}
