import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-like-button',
  templateUrl: './like-button.component.html',
  styleUrls: ['./like-button.component.scss']
})
export class LikeButtonComponent implements OnInit {



  constructor() { }

  ngOnInit(): void {
    const confettiElements:any = document.querySelectorAll('[data-confettis]')
    confettiElements && [...confettiElements].forEach(element =>
      element.addEventListener('click', this.animateButton, false)
    )
    
  }

  animateButton(e: any) {
    e.preventDefault;
    //reset animation
    e.target.classList.remove('animate');
    e.target.classList.add('animate');
    setTimeout(() => {
      e.target.classList.remove('animate');
    }, 700);
  };
}
