import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'kismat-actors-list',
  templateUrl: './actors-list.component.html',
  styleUrls: ['./actors-list.component.scss']
})
export class ActorsListComponent implements OnInit {

  @Input() actors: any = [];

  constructor() { }

  ngOnInit(): void {
  }

}
