import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { APIS } from 'src/app/apis';
import { videosrc } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';


type filetype = 'video' | 'poster' | 'audio';
type episodeOrVideo = 'videoId' | 'episodeId';

@Component({
  selector: 'kismat-file-uploads',
  templateUrl: './file-uploads.component.html',
  styleUrls: ['./file-uploads.component.scss']
})
export class FileUploadsComponent implements OnInit {

  @Input() videoId: string = "";
  @Input() isEpisodeIdOrVideoId: episodeOrVideo = "videoId";
  @Input() title: string = "Upload";
  @Input() video: videosrc = {
    name: "",
    src: "",
    img: ""
  };
  @Input() filetype: filetype = 'video';
  @Input() uploadType: string = 'video';
  @Input() fileaccept: string = 'video/*';

  @Output() response = new EventEmitter<any>();

  showProgressBar: boolean = false;
  uploadingProgress = 0;



  constructor(public http: HttpClient, public commonservice: CommonService,) { }

  ngOnInit(): void {
  }

  cahnge_file(event: any) {
    // this.uploadingType = this.uploadingTypes.video;
    this.uploadFile(event.files[0]);
  }


  uploadFile(file: File) {
    const formData = new FormData();
    formData.append("multipartFile", file);
    let url = APIS.ADMIN_UPLOAD_FILE;

    if (this.isEpisodeIdOrVideoId === 'videoId') {
      url = APIS.ADMIN_UPLOAD_FILE;
      formData.append("fileType", `{"fileType": "${this.uploadType}", "videoId": "${this.videoId}"}`);
    }
    if (this.isEpisodeIdOrVideoId === 'episodeId') {
      url = APIS.ADMIN_EPISODE_UPLOAD;
      formData.append("episodeId", this.videoId);
    }

    this.http.post(url, formData, {
      reportProgress: true,
      observe: 'events'
    }).subscribe((res: any) => {
      console.log("res", res);
      if (res.type === HttpEventType.Response) {
        if (res.body) {
          if (res.body.statusCode == 200) {
            this.response.emit(res.body.data);
            this.commonservice.snackbar(this.commonservice.titlecase(this.uploadType) + ' uploaded.', 'success');
          } else {
            this.commonservice.snackbar(res.body.message, 'error');
          }
        }
        this.showProgressBar = false;
      }

      if (res.type === HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * res.loaded / res.total);
        console.log('Progress ' + percentDone + '%');
        this.showProgressBar = true;
        this.uploadingProgress = percentDone;
      }
    })
  }


}
