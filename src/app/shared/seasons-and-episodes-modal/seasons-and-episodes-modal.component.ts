import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { episodeType, seasonType } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-seasons-and-episodes-modal',
  templateUrl: './seasons-and-episodes-modal.component.html',
  styleUrls: ['./seasons-and-episodes-modal.component.scss']
})
export class SeasonsAndEpisodesModalComponent implements OnInit {

  @Input() videoId: string = "";
  @Input() seasons: Array<any> = [];
  @Input() currentSeason: seasonType = {
    seasonId: "",
    title: ""
  };
  @Input() episodes: Array<any> = [];
  @Input() currentEpisode: episodeType = {
    title: "",
    episodeId: "",
    episodeUrl: ""
  };
  
  @Output() changeSeason = new EventEmitter<any>();
  @Output() changeEpisode = new EventEmitter<any>();

  showseasons: boolean = false;

  constructor( public commonservice: CommonService,
     public modalservice: NgbModal) { }

  ngOnInit(): void {
    console.log(this.currentEpisode);

  }

  _goToSeasons(){
    this.showseasons = true;
  }

  _changeSeason(season:any){
    this.showseasons = false;
    this.changeSeason.emit(season);
  }

  _episodeEpisode(episode:any){
    this.changeEpisode.emit(episode);
    this.closeSeasonModal();
  }

  closeSeasonModal(){
    this.modalservice.dismissAll();
  }

}
