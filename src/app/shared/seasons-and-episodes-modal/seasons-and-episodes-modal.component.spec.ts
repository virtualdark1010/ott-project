import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeasonsAndEpisodesModalComponent } from './seasons-and-episodes-modal.component';

describe('SeasonsAndEpisodesModalComponent', () => {
  let component: SeasonsAndEpisodesModalComponent;
  let fixture: ComponentFixture<SeasonsAndEpisodesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeasonsAndEpisodesModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeasonsAndEpisodesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
