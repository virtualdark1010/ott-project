import { Component, OnInit, ViewChild } from '@angular/core';
import { SwiperOptions } from 'swiper';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { RedirectService } from 'src/app/providers/redirect.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);


@Component({
  selector: 'app-scroll-bar',
  templateUrl: './scroll-bar.component.html',
  styleUrls: ['./scroll-bar.component.scss']
})
export class ScrollBarComponent implements OnInit {
  constructor(public commonservice: CommonService, public modalservice: ModalService,
    public redirectservice : RedirectService) { }

  swiperSlidesConfig: SwiperOptions = {
    // slidesPerView: 1,
    spaceBetween: 5,
    // centeredSlides:true,
    navigation: true,
    // pagination: { clickable: true }, 
    // scrollbar: { draggable: true },
    // autoplay: {
    //   delay: 2500,
    //   disableOnInteraction: true,
    // },

    // loop:true,
    // breakpoints: {
    //   320: {
    //     slidesPerView: 1.1,
    //   },
    //   680: {
    //     slidesPerView: 3.2,
    //   },
    //   992: {
    //     slidesPerView: 4.2,
    //   },
    //   1400: {
    //     slidesPerView: 6.2,
    //   }
    // }
  };

  // sliderItems: Array<Movie> = [
  //   {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-1.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-3.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-4.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-5.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-3.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   },
  //   {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-5.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }
  // ]


  ngOnInit(): void {

    
  }

}
