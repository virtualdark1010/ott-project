import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'kismat-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.scss']
})
export class DeleteModalComponent implements OnInit {

  @Input() message: string = 'Are you sure want to delete!';

  constructor(public modalService: NgbModal) {
  }

  ngOnInit(): void {
  }

  no() {
    this.modalService.dismissAll(false);
  }
  yes() {
    this.modalService.dismissAll(true);
  }


}
