import {
  Component, ElementRef, EventEmitter, HostBinding,
  HostListener, Input, OnInit, Output, SimpleChanges, ViewChild
} from '@angular/core';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { VgApiService } from '@videogular/ngx-videogular/core';
import { episodeType, seasonType, videosrc } from 'src/app/models/movie.model';
import { SeasonsAndEpisodesModalComponent } from '../seasons-and-episodes-modal/seasons-and-episodes-modal.component';

@Component({
  selector: 'kismat-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  @Input() videoId: string = "";
  @Input() video: videosrc = {
    name: "",
    src: "",
    img: ""
  };

  @Input() showVideoProgressBar: boolean = false;
  @Input() showVideoControls: boolean = false;
  @Input() mouseoverPlayPauseEvents: boolean = false;
  @Input() showOverlayPlay: boolean = false;
  @Input() showBuffer: boolean = false;
  @Input() autoplay: boolean = false;
  @Input() videoFocusOut: boolean = false;
  @Input() muted: boolean = true;
  @Input() showSeasonsDrop: boolean = false;
  @Input() showVideoBackButton: boolean = false;
  @Input() showVideoTitle: boolean = false;
  @Input() currentTime: string = "0";
  @Input() seasons: Array<any> = [];
  @Input() currentSeason: seasonType = {
    seasonId: "",
    title: ""
  };
  @Input() episodes: Array<any> = [];
  @Input() currentEpisode: episodeType = {
    title: "",
    episodeId: "",
    episodeUrl: ""
  };


  @Output() back = new EventEmitter<any>();
  @Output() ended = new EventEmitter<any>();
  @Output() seasonChange = new EventEmitter<any>();
  @Output() episodeChange = new EventEmitter<any>();

  playtimedouts: any;
  pausetimedouts: any;
  autoplayTimeout: any;

  vgapiservice: any;
  @HostBinding('class.is-buffering') isBuffering = true;

  @HostListener('window:popstate', ['$event'])
  onPopState(event: any) {
    this._goBack();
  }

  seasonModal!: NgbModalRef;

  constructor(public modalservice: NgbModal) { }
  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.autoplay && this.vgapiservice) {
      if (changes?.videoFocusOut?.currentValue) {
        this.vgapiservice.pause();
        clearTimeout(this.autoplayTimeout);
      }
      else if (!changes?.videoFocusOut?.currentValue) {
        this.vgapiservice.play();
        clearTimeout(this.autoplayTimeout);
      }
    }


    if (this.showSeasonsDrop && this.currentSeason.seasonId) {
      if (this.seasonModal) {
        if (changes?.seasons?.currentValue) {
          this.seasonModal.componentInstance.seasons = this.seasons;
        }
        if (changes?.currentSeason?.currentValue) {
          this.seasonModal.componentInstance.currentSeason = this.currentSeason;
        }
        if (changes?.episodes?.currentValue) {
          this.seasonModal.componentInstance.episodes = this.episodes;
        }
        if (changes?.currentEpisode?.currentValue) {
          this.seasonModal.componentInstance.currentEpisode = this.currentEpisode;
        }
      }

      if (this.currentEpisode.episodeId) {
        // this.video.name = this.currentEpisode.title || '';
        this.video.src = this.currentEpisode.episodeUrl || '';
      }
    }
  }

  onPlayerReady(api: VgApiService) {
    this.vgapiservice = api;
    // console.log('onPlayerReadyapi', api);

    this.vgapiservice.getDefaultMedia().subscriptions.ended.subscribe(this.videoEnded.bind(this));
    this.vgapiservice.getDefaultMedia().subscriptions.timeUpdate.subscribe(this.timeUpdate.bind(this));
    this.vgapiservice.getDefaultMedia().subscriptions.pause.subscribe(this.pauseVideo.bind(this));
    this.vgapiservice.getDefaultMedia().subscriptions.play.subscribe(this.playVideo.bind(this));

    if (this.autoplay) {
      this.vgapiservice.getDefaultMedia().subscriptions.loadedMetadata.subscribe(() => {
        this.autoplayTimeout = setTimeout(() => {
          this.initVideo();
        }, 2000);
      });
    }
  }

  initVideo() {
    if (this.currentTime != "0") {
      this.vgapiservice.getDefaultMedia().currentTime = this.currentTime;
    }
    this.vgapiservice.play();
  }

  pauseVideo() {
    this.vgapiservice.pause();
  }

  playVideo() {
    this.vgapiservice.play();
  }

  timeUpdate() {
    this.currentTime = this.vgapiservice.currentTime;
  }

  videoEnded() {
    let output = {
      currentTime: this.vgapiservice.currentTime
    }
    this.ended.emit(output);
  }


  _goBack() {
    let output = {
      currentTime: 0
    }
    if (this.video?.src) {
      output.currentTime = this.vgapiservice.currentTime;
    }

    this.back.emit(output);
  }

  openSeasons() {
    this.seasonModal = this.modalservice.open(SeasonsAndEpisodesModalComponent, {
      windowClass: 'modal-seasons',
      backdropClass: 'darken',
      backdrop: 'static',
      size: 'lg',
    })
    this.seasonModal.componentInstance.videoId = this.videoId;
    this.seasonModal.componentInstance.seasons = this.seasons;
    this.seasonModal.componentInstance.currentSeason = this.currentSeason;
    this.seasonModal.componentInstance.episodes = this.episodes;
    this.seasonModal.componentInstance.currentEpisode = this.currentEpisode;
    this.seasonModal.componentInstance.changeSeason.subscribe(this._changeSeason.bind(this));
    this.seasonModal.componentInstance.changeEpisode.subscribe(this._episodeSeason.bind(this));

    this.seasonModal.result.then((result: any) => {
      console.log(result);
    })
  }

  _changeSeason(season: any) {
    this.seasonChange.emit(season);
  }

  _episodeSeason(episode: any) {
    this.episodeChange.emit(episode);
  }

  playmedia(media: HTMLVideoElement) {
    clearTimeout(this.pausetimedouts);
    this.playtimedouts = setTimeout(() => {
      media.play();
    }, 1000);
  }

  resetmedia(media: HTMLVideoElement) {
    clearTimeout(this.playtimedouts);
    this.pausetimedouts = setTimeout(() => {
      media.src = media.src;
    }, 100)
  }

}
