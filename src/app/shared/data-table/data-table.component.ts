import { Component, EventEmitter, Injector, Input, OnInit, Output, ViewChild, ViewChildren } from '@angular/core';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

interface headers {
  lable: string;
  field: string;
  type?: string; //  date | text | image | video | arraypills | fileupload | button
  accept?: string
}

@Component({
  selector: 'kismat-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  providers: [NgbModal]
})
export class DataTableComponent implements OnInit {

  @Input() title: string = 'DataTable';
  @Input() headers: Array<headers> = [];
  @Input() data: any = [];
  @Input() editbtn: boolean = true;
  @Input() deletebtn: boolean = true;
  @Input() page: number = 1;
  @Input() pageSize: number = 10;
  @Output() onSelectItemForEdit = new EventEmitter<any>();
  @Output() onSelectItemForDelete = new EventEmitter<any>();
  @Output() onSelectFile = new EventEmitter<any>();
  @Output() onClickButton = new EventEmitter<any>();

  collectionSize: number = 0;
  dataRows: any = [];
  selectedItem: any;


  constructor(private modalService: NgbModal) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.data.forEach((x: any, index: number) => {
      x.s_no = index + 1;
    });
    this.dataRows = this.data;
    this.collectionSize = this.data.length;
    this.refreshData();
  }

  getTableColumnValue(element: any, field: any) {
    let fieldList = field.split('.');
    let newField = element;
    fieldList.forEach((f: any) => {
      newField = newField[f];
    });
    return newField
  }

  refreshData() {
    this.dataRows = this.data.map((item: any, i: number) => ({ _id: i + 1, ...item }))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  selectItem(item: any, purpose: string) {
    this.selectedItem = item;
    if (purpose === 'delete') {
      this.modalService.open(DeleteModalComponent, {
        centered: true, backdrop: 'static', keyboard: false
      }).dismissed.subscribe((confirmed: boolean) => {
        if (confirmed) {
          this.onSelectItemForDelete.emit(item)
        }
      })
    } else {
      this.onSelectItemForEdit.emit(item);
    }
  }

  changeFile(event: any, item: any) {
    this.onSelectFile.emit({ event, item });
  }

  onButton(item: any, field: any) {
    this.onClickButton.emit({ item, field }); 
  }

}
