import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { Movie } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { APIS } from 'src/app/apis';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);


@Component({
  selector: 'app-latest-trending',
  templateUrl: './latest-trending.component.html',
  styleUrls: ['./latest-trending.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LatestTrendingComponent implements OnInit {

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public commonservice: CommonService, public modalservice: ModalService,
    public redirectservice : RedirectService, private videoctrl : VideoControlsService) { }

  swiperSlidesConfig: SwiperOptions = {
    // slidesPerView: 1,
    spaceBetween: 5,
    // centeredSlides:true,
    navigation: true,
    // pagination: { clickable: true }, 
    // scrollbar: { draggable: true },
    // autoplay: {
    //   delay: 2500,
    //   disableOnInteraction: true,
    // },

    // loop:true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 3.2,
      },
      992: {
        slidesPerView: 4.2,
      },
      1400: {
        slidesPerView: 6.2,
      }
    }
  };

  // sliderItems:any= [
  //   {
  //     id: "1",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-1.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   }, {
  //     id: "2",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   }, {
  //     id: "3",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-3.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     id: "4",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-4.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     id: "5",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-5.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     id: "6",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     id: "7",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-3.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
  //   },
  //   {
  //     id: "8",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-5.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }, {
  //     id: "9",
  //     title: "Movie Name",
  //     description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
  //     img: './assets/imgs/thumb-movie-2.png',
  //     videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'

  //   }
  // ]
  
  latestAndTerndring : any = [];
  ngOnInit(): void {
    this.getLatestAndTrending();
    
  }

  getLatestAndTrending() {
    const videotype = this.videoctrl.getVideoType();
    let url = APIS.LATEST_AND_TRENDING(videotype);
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("LATESTANDTRENDING==>", res);
      if (res.statusCode == 200 && res.data.length>0) {
        this.latestAndTerndring = res.data;
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

 

}
