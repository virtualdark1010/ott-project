import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaxMovieNightComponent } from './max-movie-night.component';


describe('MaxMovieNightComponent', () => {
  let component: MaxMovieNightComponent;
  let fixture: ComponentFixture<MaxMovieNightComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaxMovieNightComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaxMovieNightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
