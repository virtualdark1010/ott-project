import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonexportsModule } from '../commonexports/commonexports.module';
import { RouterModule } from '@angular/router';

import { LatestTrendingComponent } from './latest-trending/latest-trending.component';
import { KismetSpecialsComponent } from './kismet-specials/kismet-specials.component';
import { FooterComponent } from './footer/footer.component';
import { TopTenComponent } from './top-ten/top-ten.component';
import { ExclusiveMoviesComponent } from './exclusive-movies/exclusive-movies.component';
import { SubscribePlanComponent } from './subscribe-plan/subscribe-plan.component';
import { VideoComponent } from './video/video.component';
import { ContinueWatchingComponent } from './continue-watching/continue-watching.component';
import { DataTableComponent } from './data-table/data-table.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { PillsComponent } from './pills/pills.component';
import { ActorsListComponent } from './actors-list/actors-list.component';
import { ScrollBarComponent } from './scroll-bar/scroll-bar.component';
import { MaxMovieNightComponent } from './max-movie-night/max-movie-night.component';
import { FavouriteDevicesComponent } from './favourite-devices/favourite-devices.component';
import { StreamingComponent } from './streaming/streaming.component';
import { PlayerActionComponent } from '../home/player-actions/player-actions.component';
import { LikeButtonComponent } from './like-button/like-button.component';
import { SeasonsAndEpisodesModalComponent } from './seasons-and-episodes-modal/seasons-and-episodes-modal.component';
import { FileUploadsComponent } from './file-uploads/file-uploads.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { ManageProfileComponent } from './manage-profile/manage-profile.component';
@NgModule({
  declarations: [
    LatestTrendingComponent,
    KismetSpecialsComponent,
    FooterComponent,
    TopTenComponent,
    ExclusiveMoviesComponent,
    SubscribePlanComponent,
    VideoComponent,
    ContinueWatchingComponent,
    DataTableComponent,
    DeleteModalComponent,
    PillsComponent,
    ActorsListComponent,
    ScrollBarComponent,
    MaxMovieNightComponent,
    FavouriteDevicesComponent,
    StreamingComponent,
    PlayerActionComponent,
    LikeButtonComponent,
    SeasonsAndEpisodesModalComponent,
    FileUploadsComponent,
    RecommendedComponent,
    ManageProfileComponent,
  ],
  exports: [
    LatestTrendingComponent,
    KismetSpecialsComponent,
    FooterComponent,
    TopTenComponent,
    ExclusiveMoviesComponent,
    SubscribePlanComponent,
    VideoComponent,
    ContinueWatchingComponent,
    DataTableComponent,
    DeleteModalComponent,
    PillsComponent,
    ActorsListComponent,
    ScrollBarComponent,
    MaxMovieNightComponent,
    FavouriteDevicesComponent,
    StreamingComponent,
    PlayerActionComponent,
    LikeButtonComponent,
    SeasonsAndEpisodesModalComponent,
    FileUploadsComponent,
    RecommendedComponent,
    ManageProfileComponent,
  ],
  imports: [
    CommonModule,
    CommonexportsModule,
    RouterModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
