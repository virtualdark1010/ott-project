import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KismetSpecialsComponent } from './kismet-specials.component';

describe('KismetSpecialsComponent', () => {
  let component: KismetSpecialsComponent;
  let fixture: ComponentFixture<KismetSpecialsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KismetSpecialsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KismetSpecialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
