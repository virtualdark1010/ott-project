import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { APIS } from 'src/app/apis';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-continue-watching',
  templateUrl: './continue-watching.component.html',
  styleUrls: ['./continue-watching.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContinueWatchingComponent implements OnInit {

  swiperSlidesConfig: SwiperOptions = {
    // slidesPerView: 1,
    spaceBetween: 5,
    // centeredSlides:true,
    navigation: true,
    // pagination: { clickable: true }, 
    // scrollbar: { draggable: true },
    // autoplay: {
    //   delay: 2500,
    //   disableOnInteraction: true,
    // },

    // loop:true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 4.2,
      },
      992: {
        slidesPerView: 5.2,
      },
      1400: {
        slidesPerView: 6.2,
      }
    }
  };

  userInfo: any = {};
  continueWatching: any = [];


  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public commonservice: CommonService,
    public modalservice: ModalService,
    public redirectservice: RedirectService) {
  }


  ngOnInit(): void {
    this.userInfo = this.commonservice.getuserInfo();
    console.log("userInfo==>", this.userInfo);
    this.getData();
  }

  getData() {
    this.commonservice.getMethod(APIS.GET_CONTINUE_WATCHING(this.userInfo.username)).subscribe((res: any) => {
      console.log("GET_CONTINUE_WATCHING==>", res);
      if (res.statusCode == 200 && res.data.length > 0) {
        this.continueWatching = res.data;

        this.continueWatching.forEach(async (item: any) => {
          item.completedPercentage = await this.commonservice.getCompletedPercentage(item.movieLength, item.watchDuration);
        
          item.totalMin = await this.commonservice.getVideoLengthHrsToMin(item.movieLength);
          item.completedMin = await this.commonservice.getVideoLengthSecToMin(item.watchDuration);

        });

        console.log("continueWatching==>", this.continueWatching);

      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }
}
