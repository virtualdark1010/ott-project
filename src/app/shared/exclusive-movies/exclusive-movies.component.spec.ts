import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExclusiveMoviesComponent } from './exclusive-movies.component';

describe('ExclusiveMoviesComponent', () => {
  let component: ExclusiveMoviesComponent;
  let fixture: ComponentFixture<ExclusiveMoviesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExclusiveMoviesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExclusiveMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
