import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { Movie } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { APIS } from 'src/app/apis';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-top-ten',
  templateUrl: './top-ten.component.html',
  styleUrls: ['./top-ten.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TopTenComponent implements OnInit {

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public commonservice: CommonService, public modalservice: ModalService,
    public redirectservice: RedirectService, private videoctrl: VideoControlsService) { }

  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 5,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 3.2,
      },
      992: {
        slidesPerView: 4.2,
      },
      1400: {
        slidesPerView: 6.2,
      }
    }
  };


  topTen : any = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    const videoType = this.videoctrl.getVideoType() || "";
    this.commonservice.getMethod(APIS.GET_TOP_TEN_VIDEOS(videoType)).subscribe((res: any) => {
      console.log("GET_TOP_TEN_VIDEOS==>", res);
      if (res.statusCode == 200 && res.data.length>0) {
        this.topTen = res.data;
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

}
