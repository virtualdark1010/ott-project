import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Data, Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { ModalForgotPasswordComponent } from '../modal-forgot-password/modal-forgot-password.component';
import { ModalSignupComponent } from '../modal-signup/modal-signup.component';

@Component({
  selector: 'app-modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {
  showLoginForm: boolean = false;
  modalRef: any;

  constructor(public modalservice: ModalService,
    public commonservice: CommonService,
    public router: Router) { }

  login() {
    this.showLoginForm = !this.showLoginForm;
  }

  // openSignupModal() {
  //   this.activeModal.close();
  //   const modalRef = this.modalService.open(ModalSignupComponent, {
  //     windowClass: 'modal-auth',
  //     backdropClass: 'darken',
  //     centered: true
  //   });
  // }

  // forgotPasswordModal() {
  //   this.activeModal.close();
  //   const modalRef = this.modalService.open(ModalForgotPasswordComponent, {
  //     windowClass: 'modal-auth',
  //     backdropClass: 'darken',
  //     centered: true
  //   });
  // }

  public signInForm!: FormGroup;
  public signInWith!: FormGroup;
  submitted: boolean = false;

  ngOnInit(): void {
    this.signInForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      // email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
    });

    this.signInWith = new FormGroup({
      mobilenumber: new FormControl(null, [Validators.required]),
      loginwith: new FormControl(''),
      mobileOTP: new FormControl(null),
      pin: new FormControl(null),
    });
  }

  loginWithPin() {
    if (this.signInWith.valid) {
      let url = APIS.VERIFY_MOBILE_NUMBER;
      let reqObj = {
        mobileNumber: '91' + this.signInWith.value.mobileNumber,
      }
      this.commonservice.postMethod(url, reqObj)
        .subscribe((res: Data) => {
          this.signInWith.patchValue({ loginwith: 'pin' });
        },
          err => {
            this.commonservice.snackbar(err.message, "error");
          })
    } else {
      this.commonservice.snackbar("Invalid Form", "error");
    }
    console.log("hello", this.signInWith);
  }

  loginWithOtp() {
    console.log("hello", this.signInWith);
    this.submitted = true;
    if (this.signInWith.valid) {
      let reqObj = {
        mobileNumber: '91' + this.signInWith.value.mobileNumber,
      }
      this.loginWithOtpApi(reqObj);
      this.signInWith.patchValue({ loginwith: 'otp' });
    }
    else {
      this.commonservice.snackbar("Invalid Form", "error");
    }
  }

  submitLoginWithOtpOrPin() {
    this.submitted = true;
    if (this.signInWith.valid) {
      if (this.signInWith.value.loginwith == 'otp') {
        let reqObj = {
          mobileNumber: '91' + this.signInWith.value.mobileNumber,
          mobileOTP: this.signInWith.value.mobileOTP,
        }
        this.loginWithOtpApi(reqObj);
      } else if (this.signInWith.value.loginwith == 'pin') {
        let reqObj = {
          mobileNumber: '91' + this.signInWith.value.mobileNumber,
          pin: this.signInWith.value.pin,
        }
        this.signInApi(reqObj);
      }
    }
    else {
      this.commonservice.snackbar("Invalid Form", "error");
    }
  }

  loginWithOtpApi(reqObj: any) {
    var url = APIS.USER_LOGIN_WITH_MOBILE;
    this.commonservice.postMethod(url, reqObj)
      .subscribe((res: Data) => {
        console.log("Login", res);
        if (res.statusCode == 200) {
          if (this.signInWith.value.mobileOTP) {
            this.commonservice.setuserInfo(res.data);
            this.commonservice.setToken('---');
            this.commonservice.snackbar('Login Successfully', 'success');
            this.modalservice.closeModal();
            this.router.navigate(['/home']);
          }
        } else {
          this.commonservice.snackbar(res.message, "error");
        }

      },
        err => {
          this.commonservice.snackbar(err.message, "error");
        })
  }


  onSignIn() {
    this.submitted = true;
    if (this.signInForm.valid) {
      let reqObj = this.signInForm.getRawValue();
      this.signInApi(reqObj);
    }
    else {
      this.commonservice.snackbar("Invalid Form", "error");
    }
  }


  signInApi(reqObj: any) {
    var url = APIS.USER_LOGIN;
    this.commonservice.postMethod(url, reqObj)
      .subscribe((res: Data) => {
        console.log("res::", res);
        if (res.statusCode == 200) {
          this.commonservice.setuserInfo(res.data.userdata);
          this.commonservice.setToken(res.data.accessToken);
          this.commonservice.snackbar('Login Successfully', 'success');
          this.modalservice.closeModal();
          this.router.navigate(['/home']);
        } else {
          this.commonservice.snackbar(res.message, "error");
        }
      }, err => {
        this.commonservice.snackbar(err.message, "error");
      })
  }

  // getUserDetails(userId: string) {
  //   let url = APIS.GET_USER_DETAILS_BY_ID(userId)
  //   this.commonservice.getMethod(url)
  //     .subscribe((res: Data) => {
  //       console.log("getuser", res);
  //       if (res.statusCode == 200) {
  //         this.commonservice.setuserInfo(res.data);
  //         this.router.navigate(['/home']);
  //       } else {
  //         this.commonservice.snackbar(res.message, "error");
  //       }
  //     },
  //       err => {
  //         this.commonservice.snackbar(err.message, "error");
  //       })
  // }

}
