import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailVerifivationComponent } from './mail-verifivation.component';

describe('MailVerifivationComponent', () => {
  let component: MailVerifivationComponent;
  let fixture: ComponentFixture<MailVerifivationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailVerifivationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailVerifivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
