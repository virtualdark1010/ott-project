import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/providers/common.service';
@Component({
  selector: 'kismet-mail-verifivation',
  templateUrl: './mail-verifivation.component.html',
  styleUrls: ['./mail-verifivation.component.scss']
})
export class MailVerifivationComponent implements OnInit {

  otp: FormControl = new FormControl("", [Validators.required, Validators.minLength(6), Validators.maxLength(6)]);

  config: any = {
    allowNumbersOnly: true,
    length: 6,
    isPasswordInput: false,
    placeholder: ''
  };
  constructor(
    public commonservice: CommonService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  onOtpChange(event: any) {
    this.otp.setValue(event);
  }

  verify() {
    if (this.otp.invalid) {
      this.commonservice.snackbar("Please enter valid OTP", "error");
      return;
    }
    console.log(this.otp);
  }

} 
