import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MakePaymentComponent } from './make-payment/make-payment.component';
import { LandingComponent } from './landing.component';
import { LangingLayoutComponent } from './langing-layout/langing-layout.component';
import { MailVerifivationComponent } from './mail-verifivation/mail-verifivation.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
  {
    path: '', 
    component: LandingComponent, 
    children: [
      { path: '', component: LangingLayoutComponent },
      { path: 'payment', component: MakePaymentComponent },
      { path: 'privacy', component: PrivacyPolicyComponent },
      { path: 'termsofuse', component: TermsOfUseComponent },
      { path: 'contactus', component: ContactusComponent },
    ]
  },
  { path: 'verify-otp/:userid', component: MailVerifivationComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
