import { Component, OnInit } from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-forgot-password',
  templateUrl: './modal-forgot-password.component.html',
  styleUrls: ['./modal-forgot-password.component.scss']
})
export class ModalForgotPasswordComponent implements OnInit {
  showOTPForm:boolean = false;
  resetPass:boolean = false;
  constructor( public activeModal: NgbActiveModal, private modalService: NgbModal) { }

  getOtp(){
    this.showOTPForm = !this.showOTPForm;
  }
  resetPassword(){
    this.resetPass = !this.resetPass;
  }

  ngOnInit(): void {
  }

}
