import { Component, Input, OnInit } from '@angular/core';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

@Component({
  selector: 'app-explore-all',
  templateUrl: './explore-all.component.html',
  styleUrls: ['./explore-all.component.scss']
})
export class ExploreAllComponent implements OnInit {
  @Input() data:any;
  @Input() title:string = '';
  constructor(public modalservice: ModalService,public videocontrolservice: VideoControlsService,
    public commonservice: CommonService,) { }


  ngOnInit(): void {
  }

}
