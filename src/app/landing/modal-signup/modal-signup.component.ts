import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Data } from '@angular/router';
// import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
// import { VideoControlsService } from 'src/app/providers/video-controls.service';
// import { ModalLoginComponent } from '../modal-login/modal-login.component';
import { NgxIpComponent } from 'ngx-ip-address'
@Component({
  selector: 'app-modal-signup',
  templateUrl: './modal-signup.component.html',
  styleUrls: ['./modal-signup.component.scss']
})
export class ModalSignupComponent implements OnInit { 
  public signUpForm!: FormGroup;
  submitted: boolean = false;
  ipAddress: string = "";

  constructor(public modalservice: ModalService,
    public commonservice: CommonService,private http:HttpClient) { }

  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      firstName: new FormControl("", [Validators.required]),
      lastName: new FormControl("", [Validators.required]),
      userName: new FormControl("", [Validators.required]),
      email: new FormControl("", [Validators.required, Validators.email]),
      countrycode: new FormControl("",[Validators.required]),
      mobileNumber: new FormControl("", []),
      password: new FormControl("", [Validators.required]),
      role: new FormControl(["user"])
    });

    this.getIp();
  }

  // openSigninModal(){
  //   this.activeModal.close();
  //   const modalRef = this.modalService.open(ModalLoginComponent,{
  //     windowClass: 'modal-auth',
  //     backdropClass:'darken',
  //     centered:true
  //   });
  // }
 
  async getIp(){ 
    // this.commonservice.getIPAddress().subscribe(res=>{
    //   this.ipAddress = res.ip;
    // });

    console.log("NgxIpComponent", NgxIpComponent )
  }

  onRegister() {
    this.submitted = true;
    console.log(this.signUpForm);
    if (this.signUpForm.valid) {
      var url = APIS.USER_REGISTER_OR_UPDATE;
      let user = this.signUpForm.getRawValue();
      this.commonservice.postMethod(url, user)
        .subscribe((res: Data) => {
          console.log("res==>", res);
          if (res.statusCode == 200) {
            this.submitted = false;
            this.signUpForm.reset();
            this.commonservice.snackbar("Registered Succesfully");
          } else {
            this.commonservice.snackbar(res.message, 'error')
          }
        }, err => {
          console.log("err==>", err);
          this.commonservice.snackbar(err.error.message, 'error')
        })
    } else {
      this.commonservice.snackbar("Form Invalid", 'error')
    }
  }


}
