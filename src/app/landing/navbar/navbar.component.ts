import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { ModalLoginComponent } from '../modal-login/modal-login.component';
import { ModalSignupComponent } from '../modal-signup/modal-signup.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor( public commonservice:CommonService, public modalservice: ModalService,
    public videocontrolservice: VideoControlsService) { }

  // openLoginModal(){
  //   const modalRef = this.modalService.open(ModalLoginComponent,{
  //     windowClass: 'modal-auth',
  //     backdropClass:'darken',
  //     centered:true
  //   });

  //   // this.videocontrolservice.pauseVideo();
  // }

  // openSignUpModal(){
  //   const modalRef = this.modalService.open(ModalSignupComponent ,{
  //     windowClass: 'modal-auth',
  //     backdropClass:'darken',
  //     centered:true
  //   });

  //   // this.videocontrolservice.pauseVideo();
  // }



  ngOnInit(): void {
  }

}
