import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LangingLayoutComponent } from './langing-layout.component';

describe('LangingLayoutComponent', () => {
  let component: LangingLayoutComponent;
  let fixture: ComponentFixture<LangingLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LangingLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LangingLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
