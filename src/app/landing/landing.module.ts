import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonexportsModule } from '../commonexports/commonexports.module';
import { SharedModule } from '../shared/shared.module';

import { LandingRoutingModule } from './landing-routing.module';
import { NgxIpModule } from 'ngx-ip-address'

import { LandingComponent } from './landing.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { ModalLoginComponent } from './modal-login/modal-login.component';
import { ModalSignupComponent } from './modal-signup/modal-signup.component';
import { DownloadWatchOfflineComponent } from './download-watch-offline/download-watch-offline.component';
import { RecommendedMoviesComponent } from './recommended-movies/recommended-movies.component';
import { ModalForgotPasswordComponent } from './modal-forgot-password/modal-forgot-password.component';
import { LangingLayoutComponent } from './langing-layout/langing-layout.component';
import { VideoComponent } from '../shared/video/video.component';
import { ExploreAllComponent } from './exploreAll/explore-all.component';
import { HttpClientModule } from '@angular/common/http';
import { MailVerifivationComponent } from './mail-verifivation/mail-verifivation.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsOfUseComponent } from './terms-of-use/terms-of-use.component';
import { ContactusComponent } from './contactus/contactus.component';

@NgModule({
  declarations: [
    LandingComponent,
    NavbarComponent,
    HeaderComponent,
    ModalSignupComponent,
    ModalLoginComponent,
    DownloadWatchOfflineComponent,
    RecommendedMoviesComponent,
    ModalForgotPasswordComponent,
    LangingLayoutComponent,
    ExploreAllComponent,
    MailVerifivationComponent,
    PrivacyPolicyComponent,
    TermsOfUseComponent,
    ContactusComponent,
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    SharedModule,
    CommonexportsModule,
    HttpClientModule,
    NgxIpModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LandingModule { }
