import { Component, OnInit, ViewChild } from '@angular/core';

import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual } from 'swiper';
import Swiper, { Navigation, Pagination } from 'swiper';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination]);

@Component({
  selector: 'app-recommended-movies',
  templateUrl: './recommended-movies.component.html',
  styleUrls: ['./recommended-movies.component.scss']
})
export class RecommendedMoviesComponent implements OnInit {

  @ViewChild('swiperContainer', { static: false }) swiperContainer?:SwiperComponent;
  constructor() { }

  swiperSlidesConfig: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 0,
    // centeredSlides:true,
    navigation: false,
    pagination: { clickable: true }, 
    // scrollbar: { draggable: true },
  };

  // slideNext(){
  //   console.log(this.swiperContainer?.swiperRef)
  //   this.swiperContainer?.swiperRef.slideNext();
  // }
  // slidePrev(){
  //   this.swiperContainer?.swiperRef.slidePrev();
  // }


  sliderItems = [
    {
      tags: "image Alt or title",
      img: './assets/imgs/recommended-video-banner-1.png'
    }, {
      tags: "image Alt or title",
      img: './assets/imgs/recommended-video-banner-2.png'
    }, {
      tags: "image Alt or title",
      img: './assets/imgs/recommended-video-banner-3.png'
    }, {
      tags: "image Alt or title",
      img: './assets/imgs/recommended-video-banner-4.png'
    }
  ]

  ngOnInit(): void {
  }

}
