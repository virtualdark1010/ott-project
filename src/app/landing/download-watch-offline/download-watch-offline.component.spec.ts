import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DownloadWatchOfflineComponent } from './download-watch-offline.component';

describe('DownloadWatchOfflineComponent', () => {
  let component: DownloadWatchOfflineComponent;
  let fixture: ComponentFixture<DownloadWatchOfflineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DownloadWatchOfflineComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadWatchOfflineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
