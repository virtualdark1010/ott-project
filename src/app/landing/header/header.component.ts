import { Component, HostListener, OnInit, ViewChild } from '@angular/core';

import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual } from 'swiper';
import Swiper, { Navigation, Pagination } from 'swiper';
import { Movie } from 'src/app/models/movie.model';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { ModalService } from 'src/app/providers/modal.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination]);

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {


  // swiperSlidesConfig: SwiperOptions = {
  //   slidesPerView: 1,
  //   spaceBetween: 0,
  //   // centeredSlides:true,
  //   navigation: true,
  //  // pagination: { clickable: true },

  //    scrollbar: { draggable: true },
  // };

  // activeSildeIndex: number = 0;

  // slideNext(){
  //   console.log(this.swiperContainer?.swiperRef)
  //   this.swiperContainer?.swiperRef.slideNext();
  // }
  // slidePrev(){
  //   this.swiperContainer?.swiperRef.slidePrev();
  // }

  sliderItems: Array<Movie> = [
    {
      title: "Spider man No way Home",
      description: "Eight year-old Bat man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number. He wants to talk to his father, who his mother tells... ",
      img: './assets/imgs/batman-hero-24003474.jpg',
      videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
    }, 
    {
      title: "Spider man No way Home",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number. He wants to talk to his father, who his mother tells... ",
      img: './assets/imgs/slider-img/slider-02.png',
      videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
    }, 
    {
      title: "Spider man No way Home2",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/slider-img/slider-01.png',
      videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
    },
    {
      title: "Spider man No way Home2",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/slider-img/slider-01.png',
      videourl: 'https://testvideosbuc.s3.ap-south-1.amazonaws.com/final.mp4'
    },
  ]

  // videoFocusOut = false;

  // @HostListener('window:scroll', ['$event'])
  // track(event: any) {
  //   if (window.pageYOffset > 400) {
  //     this.videoFocusOut = true;
  //   } else {
  //     this.videoFocusOut = false;
  //   }
  // }

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public videocontrolservice: VideoControlsService,
    public modalservice: ModalService,) { }

  ngOnInit(): void {
    // this.videocontrolservice.videoController$.subscribe((data) => {
    //   if (data?.action === 'play') {
    //     this.videoFocusOut = false;
    //   } else if (data?.action === 'pause') {
    //     this.videoFocusOut = true;
    //   }
    // }); 
  }




}
