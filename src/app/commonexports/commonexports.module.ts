import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgOtpInputModule } from 'ng-otp-input';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DirectiveModule } from '../Directives/directive.module';

import { SwiperModule } from 'swiper/angular';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//videoangular 
import { VgCoreModule } from '@videogular/ngx-videogular/core';
import { VgControlsModule } from '@videogular/ngx-videogular/controls';
import { VgOverlayPlayModule } from '@videogular/ngx-videogular/overlay-play';
import { VgBufferingModule } from '@videogular/ngx-videogular/buffering';


const exportModules = [
  CommonModule,
  SwiperModule,
  NgbModule,
  NgOtpInputModule,
  FormsModule,
  ReactiveFormsModule,
  HttpClientModule,
  VgCoreModule,
  VgControlsModule,
  VgOverlayPlayModule,
  VgBufferingModule,
  DirectiveModule,
]

@NgModule({
  imports: exportModules,
  exports: exportModules
})
export class CommonexportsModule { }
