import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTvShowsComponent } from './home-tv-shows.component';

describe('HomeTvShowsComponent', () => {
  let component: HomeTvShowsComponent;
  let fixture: ComponentFixture<HomeTvShowsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeTvShowsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTvShowsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
