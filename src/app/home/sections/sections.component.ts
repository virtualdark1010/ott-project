import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { APIS } from 'src/app/apis';
import { RedirectService } from 'src/app/providers/redirect.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);


@Component({
  selector: 'kismet-sections',
  templateUrl: './sections.component.html',
  styleUrls: ['./sections.component.scss']
})
export class SectionsComponent implements OnInit {

  sections: any = [];
  
  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 5,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 4.2,
      },
      992: {
        slidesPerView: 5.2,
      },
      1400: {
        slidesPerView: 7.2,
      }
    }
  };
  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  
  constructor(public commonservice: CommonService, private videoctrl: VideoControlsService,
    public redirectservice: RedirectService) { }

  ngOnInit(): void {
    this.getSections();
  }
  
    getSections() {
      let videoType = this.videoctrl.getSectionVideoType();
    var url = APIS.GET_ALL_SECTIONS_BY_VIDEO_TYPE(videoType);
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("GET_ALL_SECTIONS_BY_VIDEO_TYPE", res);
      if (res.statusCode == 200) {
        this.sections = res.data;
      }
      else {
        this.commonservice.snackbar(res.message, 'error');
      }
    }, err => {
      console.log("err", err);
      this.commonservice.snackbar(err.message, 'error');
    }
    )
  }
}
