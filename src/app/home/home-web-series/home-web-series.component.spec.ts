import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeWebSeriesComponent } from './home-web-series.component';

describe('HomeWebSeriesComponent', () => {
  let component: HomeWebSeriesComponent;
  let fixture: ComponentFixture<HomeWebSeriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeWebSeriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeWebSeriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
