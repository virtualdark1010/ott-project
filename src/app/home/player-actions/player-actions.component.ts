import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';
import { CommonService } from '../../providers/common.service';
import { MoreInfoComponent } from '../more-info/more-info.component';


@Component({
  selector: 'app-player-actions',
  templateUrl: './player-actions.component.html',
  styleUrls: ['./player-actions.component.scss']
})
export class PlayerActionComponent implements OnInit, AfterViewInit {
  @Input() videoId: string = '';
  @Input() flag?: boolean = false;
  @Input() viewMore: boolean = false;
  @Input() showlove: boolean = false;

  userInfo: any;
  counter = 0;
  clickable = ["like", "dislike", "love"];
  isLike: boolean = false;
  isLove: boolean = false;
  isDisLike: boolean = false;


  constructor(private commonService: CommonService,
  private modalService: NgbModal) {
  }
  ngOnInit(): void {
    this.userInfo = this.commonService.getuserInfo();
  }

  userAction(event: any) {
    if(event.target.id == 'moreInfo'){
      this.openMoreInfoPop();
      return;
    }

    if (this.clickable.findIndex(x => x == event.target.id) > -1) {
      let reqData = {
        "userName": this.userInfo.username,
        "videoId": this.videoId,
        "videoPreference": event.target.id,
        "watchedDuration": "string"
      }
      let url = APIS.VIDEO_PREFERENCE;
      this.commonService.postMethod(url, reqData).subscribe(res => {
        this.getVideoPreference();
      })
    }
  }

  ngAfterViewInit() {
    if (this.flag) {
      this.getVideoPreference();
    }
  }
  getVideoPreference() {
    let url = APIS.GET_VIDEO_PREFERENCE(this.userInfo.username);
    this.commonService.getMethod(url).subscribe(res => {
      this.isDisLike = false;
      this.isLike = false;
      this.isLove = false;
      res.data.map((x: any) => {
        if (x.videoId == this.videoId) {
          if (x.videoFlag == 'like')
            this.isLike = true;
          else if (x.videoFlag == 'dislike')
            this.isDisLike = true;
          else if (x.videoFlag == 'love')
            this.isLove = true;
        }
      })
    })
  }



  openMoreInfoPop(){
   const modalRef = this.modalService.open(MoreInfoComponent, {
      centered: true,
      size: 'lg',
      windowClass:"modal-more-info"
    })
    modalRef.componentInstance.videoId = this.videoId;

  }



}

