import { Component, HostListener, OnInit, ViewChild } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { episodeType, Movie, seasonType } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';
import { ActivatedRoute } from '@angular/router';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);
import { APIS } from 'src/app/apis';
import { RedirectService } from 'src/app/providers/redirect.service';
import { NavigationStart, Router } from '@angular/router';
import { EncrDecrService } from 'src/app/providers/encr-decr.service';

@Component({
  selector: 'app-watch-video',
  templateUrl: './watch-video.component.html',
  styleUrls: ['./watch-video.component.scss']
})
export class WatchVideoComponent implements OnInit {
  videoId: any;
  video: any = {};

  currentVideo = {
    name: '',
    src: '',
    img: ''
  };

  currentTime: string = "0";
  showSeasonsDrop: boolean = false;
  userInfo: any;
  seasons: any = [];
  currentSeason: seasonType = {
    title: null,
    seasonId: null
  };
  episodes: any = [];
  currentEpisode: episodeType = {
    title: null,
    episodeId: null,
    episodeUrl: "",
  }

  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 10,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 3.2,
      },
      992: {
        slidesPerView: 4.2,
      },
      1400: {
        slidesPerView: 6.2,
      }
    }
  };

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;

  constructor(public commonservice: CommonService,
    public activateRoute: ActivatedRoute,
    private router: Router,
    public redirectservice: RedirectService,
    private encrDecrService: EncrDecrService) {
    activateRoute.params.subscribe(params => {
      console.log("params", params);
      this.videoId = params.videoId;
      this.getVideoDetailsById();
    });
  }

  ngOnInit(): void {
    this.userInfo = this.commonservice.getuserInfo();
  }

  getVideoDetailsById() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId)).subscribe(
      res => {
        this.video = res.data
        console.log("data", res);
        this.currentVideo = {
          name: this.video.title,
          src: this.video?.videoSource?.videoUrl,
          img: this.video?.videoSource?.thumbnailUrl
        }
        this.checkItsSeasonType();
      },
      error => {
        console.log("error", error);
      }
    )
  }

  checkItsSeasonType() {
    if (['web series', 'tv shows', 'series'].includes(this.video.videoType)) {
      this.showSeasonsDrop = true;
      this.activateRoute.queryParamMap.subscribe((qparams: any) => {
        if (qparams.params.data) {
          let query_params = JSON.parse(this.encrDecrService.get(qparams.params.data));
          this.currentSeason.seasonId = query_params.seasonId;
          this.currentEpisode.episodeId = query_params.episodeId;
          this.getSeasons();
        }
      });
    }
  }

  getSeasons() {
    this.commonservice.getMethod(APIS.GET_SEASONS_BY_VIDEOID(this.videoId))
      .subscribe((res: any) => {
        console.log("GET_SEASONS_BY_VIDEOID==>", res);
        if (res.statusCode == 200) {
          this.seasons = res.data;
          if (this.seasons.length > 0) {
            let selectedSeason = this.seasons.filter((season: any) => season.seasonId == this.currentSeason.seasonId)[0];
            this.seasonChange(selectedSeason);
          }
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  seasonChange(season: any) {
    this.currentSeason = {
      title: season.title,
      seasonId: season.seasonId
    }
    this.getEpisodesBySeasonId();
  }

  getEpisodesBySeasonId() {
    this.commonservice.getMethod(APIS.GET_ALL_EPISODES_BY_SEASION_ID(this.currentSeason.seasonId))
      .subscribe((res: any) => {
        console.log("GET_ALL_EPISODES_BY_SEASION_ID==>", res);
        if (res.statusCode == 200) {
          this.episodes = res.data;
          if (this.episodes.length > 0) {
            if (this.currentEpisode.episodeId) {
              let selectedEpisode = this.episodes.filter((episode: any) => episode.episodeId == this.currentEpisode.episodeId)[0];
              this.episodeChange(selectedEpisode);
            } else {
              this.episodeChange(this.episodes[0]);
            }
          }
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  episodeChange(episode: any) {
    this.currentEpisode = {
      title: episode.title,
      episodeId: episode.episodeId,
      episodeUrl: episode.episodeUrl
    }
  }

  goBack(event: any) {
    console.log("event:: back", event);
    this.currentTime = event.currentTime;
    this.updateVideoTime();
    this.redirectservice.preview(this.videoId);
  }

  ended(event: any) {
    console.log("event::", event);
    this.currentTime = event.currentTime;
    this.updateVideoTime();
    this.redirectservice.preview(this.videoId);
  }

  updateVideoTime() {
    let req_obj = {
      userName: this.userInfo.username,
      videoId: this.videoId,
      watchedDuration: this.currentTime ? this.currentTime.toString() : "0",
      seasonId: this.currentSeason.seasonId,
      episodeId: this.currentEpisode.episodeId
    }
    this.commonservice.postMethod(APIS.SAVE_CONTINUE_WATCHING, req_obj).subscribe(
      (res) => {
        console.log("res", res);
      }
    )
  }

}
