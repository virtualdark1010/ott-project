import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeNavbarComponent } from './home-navbar/home-navbar.component';
import { CommonexportsModule } from '../commonexports/commonexports.module';
import { HomeHeaderComponent } from './home-header/home-header.component';
import { SharedModule } from '../shared/shared.module';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeMoviesComponent } from './home-movies/home-movies.component';
import { HomeTvShowsComponent } from './home-tv-shows/home-tv-shows.component';
import { HomeWebSeriesComponent } from './home-web-series/home-web-series.component';
import { NewAndPopularComponent } from './new-and-popular/new-and-popular.component';
import { SettingsComponent } from './settings/settings.component';
import { SearchComponent } from './search/search.component';
import { WatchVideoComponent } from './watch-video/watch-video.component';
import { PreviewComponent } from './preview/preview.component';
import { KismetGenresComponent } from './kismet-genres/kismet-genres.component';
import { MoreInfoComponent } from './more-info/more-info.component';
import { HomeGenresComponent } from './home-genres/home-genres.component';
import { SectionsComponent } from './sections/sections.component';
import { ChangeAccountComponent } from './settings/change-account/change-account.component';
import { ChangePasswordComponent } from './settings/change-password/change-password.component';
import { ChangeMobileComponent } from './settings/change-mobile/change-mobile.component';
import { EpisodesComponent } from './episodes/episodes.component';

@NgModule({
  declarations: [
    HomeComponent,
    HomeNavbarComponent,
    HomeHeaderComponent,
    HomeLayoutComponent,
    HomeMoviesComponent,
    HomeTvShowsComponent,
    HomeWebSeriesComponent,
    NewAndPopularComponent,
    SettingsComponent,
    SearchComponent,
    WatchVideoComponent,
    PreviewComponent,
    KismetGenresComponent,
    MoreInfoComponent,
    HomeGenresComponent,
    SectionsComponent,
    ChangeAccountComponent,
    ChangePasswordComponent,
    ChangeMobileComponent,
    EpisodesComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    CommonexportsModule,
    SharedModule
  ]
})
export class HomeModule { }
