import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HomeComponent } from './home.component';
import { HomeMoviesComponent } from './home-movies/home-movies.component';
import { HomeTvShowsComponent } from './home-tv-shows/home-tv-shows.component';
import { HomeWebSeriesComponent } from './home-web-series/home-web-series.component';
import { NewAndPopularComponent } from './new-and-popular/new-and-popular.component';
import { SettingsComponent } from './settings/settings.component';
import { WatchVideoComponent } from './watch-video/watch-video.component';
import { PreviewComponent } from './preview/preview.component';
import { KismetGenresComponent } from './kismet-genres/kismet-genres.component';
import { HomeGenresComponent } from './home-genres/home-genres.component';
import { ManageProfileComponent } from '../shared/manage-profile/manage-profile.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { path: "", component: HomeLayoutComponent },
      { path: "movies", component: HomeMoviesComponent },
      { path: "tvshows", component: HomeTvShowsComponent },
      { path: "webseries", component: HomeWebSeriesComponent },
      { path: "newandpopular", component: NewAndPopularComponent },
      { path: "preview/:videoId", component: PreviewComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'kismet-genres', component: KismetGenresComponent },
      { path: "genre/:id", component: HomeGenresComponent },
      { path: "manage-profile", component: ManageProfileComponent },
      {
        path: "live",
        loadChildren: () => import('./live/live.module').then(m => m.LiveModule),
      },
    ]
  },
  { path: "watchnow/:videoId", component: WatchVideoComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
