import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KismetGenresComponent } from './kismet-genres.component';

describe('KismetGenresComponent', () => {
  let component: KismetGenresComponent;
  let fixture: ComponentFixture<KismetGenresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KismetGenresComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KismetGenresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
