import { Component, Input, OnInit } from '@angular/core';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

@Component({
  selector: 'kismet-genres',
  templateUrl: './kismet-genres.component.html',
  styleUrls: ['./kismet-genres.component.scss']
})
export class KismetGenresComponent implements OnInit {
 sections: any = [];
  
  
  constructor(public commonservice: CommonService, private videoctrl: VideoControlsService) { }

  ngOnInit(): void {
    this.getSections();
  }
  
    getSections() {
      let videoType = this.videoctrl.getVideoType();
    var url = APIS.GET_ALL_SECTIONS_BY_VIDEO_TYPE(videoType);
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("GET_ALL_SECTIONS_BY_VIDEO_TYPE", res);
      if (res.statusCode == 200) {
        this.sections = res.data;
      }
      else {
        this.commonservice.snackbar(res.message, 'error');
      }
    }, err => {
      console.log("err", err);
      this.commonservice.snackbar(err.message, 'error');
    }
    )
  }
}
