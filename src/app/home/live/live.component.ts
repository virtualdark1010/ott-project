import { Component, OnInit } from '@angular/core';

// import Swiper core and required modules
import SwiperCore, { SwiperOptions, Navigation, Pagination, Scrollbar, A11y } from 'swiper';

// install Swiper modules
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss']
})
export class LiveComponent implements OnInit {

  active=1;
  constructor() { }

  config: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 50,
    navigation: false,
    pagination: false,
    
    breakpoints:{
      
      '768': {
        slidesPerView: 2.2,
        spaceBetween: 40
      },
      '1200': {
        slidesPerView: 3.2,
        spaceBetween: 50
      },
      '1700': {
        slidesPerView: 4.2,
        spaceBetween: 50
      }
    }
  };

  onSlideChange() {
    console.log('slide change');
  }

  ngOnInit(): void {
  }


}
