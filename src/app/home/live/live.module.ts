import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LiveRoutingModule } from './live-routing.module';
import { LiveComponent } from './live.component';
import { CommonexportsModule } from '../../commonexports/commonexports.module';
import { RouterModule } from '@angular/router';
import { LiveVideoPlayerComponent } from './live-video-player/live-video-player.component';


@NgModule({
  declarations: [
    LiveComponent,
    LiveVideoPlayerComponent
  ],
  imports: [
    CommonModule,
    CommonexportsModule,
    RouterModule,
    LiveRoutingModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class LiveModule { }
