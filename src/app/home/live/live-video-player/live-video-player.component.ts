import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
declare const videojs: any;
@Component({
  selector: 'kismet-live-video-player',
  templateUrl: './live-video-player.component.html',
  styleUrls: ['./live-video-player.component.scss']
})
export class LiveVideoPlayerComponent implements OnInit {
  @Input() videourl: string = "https://video.cycloud.pw/hls/stream.m3u8";
  @Input() posterurl: string = "assets/imgs/thumb-movie-v-1.png";
  public player: any;

  @ViewChild("livevideoplayer") livevideoplayer!: ElementRef;

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    // this.player = videojs(document.getElementById('videoplayer'));
    this.player = videojs(this.livevideoplayer.nativeElement);
  }

  getDuration(player: any) {
    var seekable = player.seekable();
    return seekable && seekable.length ? seekable.end(0) - seekable.start(0) : 0;
  }

  ngOnDestroy() {
    this.player.dispose();
  }

}