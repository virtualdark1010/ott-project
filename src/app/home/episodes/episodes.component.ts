import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';
import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'kismat-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent implements OnInit {

  @Input() seasonId: any = '';
  @Input() title: string = "Episodes";
  @Input() episodes: Array<any> = [];
  
  @Output() selectedEpisode = new EventEmitter<any>();

  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 5,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 4.2,
      },
      992: {
        slidesPerView: 5.2,
      },
      1400: {
        slidesPerView: 7.2,
      }
    }
  };

  constructor(public commonservice: CommonService) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.seasonId){
      this.getEpisodesBySeasonId();
    }
  }

  getEpisodesBySeasonId() {
    this.commonservice.getMethod(APIS.GET_ALL_EPISODES_BY_SEASION_ID(this.seasonId))
      .subscribe((res: any) => {
        console.log("GET_ALL_EPISODES_BY_SEASION_ID==>", res);
        if (res.statusCode == 200) {
          this.episodes = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  selectEpisode(episode:any){
    this.selectedEpisode.emit(episode);
  }

}
