import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-change-account',
  templateUrl: './change-account.component.html',
  styleUrls: ['./change-account.component.scss']
})
export class ChangeAccountComponent implements OnInit {
  submitted : boolean = false;
  payload: FormGroup = this.fb.group({
    email: ['', [Validators.required]]
  })

  constructor(private fb: FormBuilder, public modalService: NgbModal) { }

  ngOnInit(): void {
  }

  verifyEmail(){  
    this.submitted = true;
    console.log(this.payload)

  }

}
