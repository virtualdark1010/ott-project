import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeAccountComponent } from './change-account/change-account.component';
import { ChangeMobileComponent } from './change-mobile/change-mobile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  changeAccount() {
    const modalRef = this.modalService.open(ChangeAccountComponent, {
      centered: true,
      size: 'lg',
      windowClass: ""
    })
  }
  changePassword() {
    const modalRef = this.modalService.open(ChangePasswordComponent, {
      centered: true,
      size: 'lg',
      windowClass: "general"
    })
  }
  changeMobile() {
    const modalRef = this.modalService.open(ChangeMobileComponent, {
      centered: true,
      size: 'lg',
      windowClass: "general"
    })
  }

}
