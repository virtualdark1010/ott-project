import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  submitted: boolean = false;
  payload: FormGroup = this.fb.group({
    oldPassword: ['', [Validators.required]],
    newPassword: ['', [Validators.required]],
  })

  constructor(private fb: FormBuilder, public modalService: NgbModal) { }

  ngOnInit(): void {
  }

  submit() {
    this.submitted = true;
    console.log(this.payload)

  }

}
