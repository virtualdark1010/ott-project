import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-change-mobile',
  templateUrl: './change-mobile.component.html',
  styleUrls: ['./change-mobile.component.scss']
})
export class ChangeMobileComponent implements OnInit {

  submitted : boolean = false;
  payload: FormGroup = this.fb.group({
    mobileNumber: ['', [Validators.required]]
  })

  constructor(private fb: FormBuilder, public modalService: NgbModal) { }

  ngOnInit(): void {
  }

  submit(){  
    this.submitted = true;
    console.log(this.payload)

  }

}
