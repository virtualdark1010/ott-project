import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-more-info',
  templateUrl: './more-info.component.html',
  styleUrls: ['./more-info.component.scss']
})
export class MoreInfoComponent implements OnInit {
 
  @Input() videoId: string = '';

  videoActors: any = [];

  constructor(private commonService: CommonService, public activeModal: NgbActiveModal,) { }

  ngOnInit(): void {
    this.getActorsByVideoId();
  }

  getActorsByVideoId() {
    this.commonService.getMethod(APIS.ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID==>", res);
        if (res.statusCode == 200) {
          this.videoActors = res.data;
        } else {
          this.commonService.snackbar(res.message, 'error')
        }
      })
  }

}
