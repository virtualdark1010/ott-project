import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual, Autoplay, Navigation, Pagination } from 'swiper';
import { episodeType, Movie, seasonType } from 'src/app/models/movie.model';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { ActivatedRoute } from '@angular/router';
import { EncrDecrService } from 'src/app/providers/encr-decr.service';
import { APIS } from 'src/app/apis';
import { RedirectService } from 'src/app/providers/redirect.service';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination, Autoplay]);

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PreviewComponent implements OnInit {

  videoId: any;
  video: any = {};

  currentVideo = {
    name: '',
    src: '',
    img: ''
  }

  swiperSlidesConfig: SwiperOptions = {
    spaceBetween: 10,
    navigation: true,
    breakpoints: {
      320: {
        slidesPerView: 1.1,
      },
      680: {
        slidesPerView: 3.2,
      },
      992: {
        slidesPerView: 4.2,
      },
      1400: {
        slidesPerView: 6.2,
      }
    }
  };

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;

  ownVideo = false;
  showSeasonsDrop = false;

  seasons: any = [];
  currentSeason: seasonType = {
    title: null,
    seasonId: null
  };
  episodes: any = [];
  currentEpisode: episodeType = {
    title: null,
    episodeId: null,
    episodeUrl: "",
  }
  constructor(public commonservice: CommonService,
    public activateRoute: ActivatedRoute,
    public redirectservice: RedirectService) {
    activateRoute.params.subscribe(params => {
      this.videoId = params.videoId;

      this.getVideoDetailsById();
    })
  }

  ngOnInit(): void {
  }


  getVideoDetailsById() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId)).subscribe(
      res => {
        this.video = res.data
        console.log("data", res);
        this.currentVideo = {
          name: this.video.title,
          src: this.video?.videoSource?.trailerUrl,
          img: this.video?.videoSource?.thumbnailUrl
        }

        this.ownVideo = true

        this.checkItsSeasonType();

      },
      error => {
        console.log("error", error);
      }
    )
  }

  checkItsSeasonType() {
    if (['web series', 'tv shows', 'series'].includes(this.video.videoType)) {
      this.showSeasonsDrop = true;
      this.getSeasons();
    }
  }

  getSeasons() {
    this.commonservice.getMethod(APIS.GET_SEASONS_BY_VIDEOID(this.videoId))
      .subscribe((res: any) => {
        console.log("GET_SEASONS_BY_VIDEOID==>", res);
        if (res.statusCode == 200) {
          this.seasons = res.data;
          if (this.seasons.length > 0) {
            this.seasonChange(this.seasons[0]);
          }
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  seasonChange(season: any) {
    this.currentSeason = {
      title: season.title,
      seasonId: season.seasonId
    }
    // this.getEpisodesBySeasonId();
  }

  selectedEpisode(episode: any) {
    console.log("event==>", event);
    this.currentEpisode = {
      title: episode.title,
      episodeId: episode.episodeId,
      episodeUrl: episode.episodeUrl
    }
    this.startWatching();
  }

  startWatching() {
    if (this.showSeasonsDrop && this.currentSeason.seasonId) {
      let params = {
        seasonId: this.currentSeason.seasonId,
        episodeId: this.currentEpisode.episodeId
      }
      this.redirectservice.watchNow(this.videoId, params);
    } else {
      this.redirectservice.watchNow(this.videoId);
    }


  }

  // getEpisodesBySeasonId() {
  //   this.commonservice.getMethod(APIS.GET_ALL_EPISODES_BY_SEASION_ID(this.currentSeason.seasonId))
  //     .subscribe((res: any) => {
  //       console.log("GET_ALL_EPISODES_BY_SEASION_ID==>", res);
  //       if (res.statusCode == 200) {
  //         this.episodes = res.data;
  //         if (this.episodes.length > 0) {
  //           this.episodeChange(this.episodes[0]);
  //         }
  //       } else {
  //         this.commonservice.snackbar(res.message, 'error')
  //       }
  //     })
  // }

  // episodeChange(episode: any) {
  //   this.currentEpisode = {
  //     title: episode.title,
  //     episodeId: episode.episodeId,
  //     episodeUrl: episode.episodeUrl
  //   }
  // }

}
