import { Component, HostListener, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { SwiperOptions } from 'swiper';
import { SwiperComponent } from 'swiper/angular';

import SwiperCore, { Virtual } from 'swiper';
import Swiper, { Navigation, Pagination } from 'swiper';
import { Movie } from 'src/app/models/movie.model';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { CommonService } from 'src/app/providers/common.service';
import { APIS } from 'src/app/apis';
// install Swiper modules
SwiperCore.use([Virtual, Navigation, Pagination]);

@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeHeaderComponent implements OnInit {
  @Input() fullHeight: boolean = false;

  @ViewChild('swiperContainer', { static: false }) swiperContainer?: SwiperComponent;
  constructor(public redirectservice: RedirectService, public commonservice: CommonService,
    private videoctrl: VideoControlsService) { }

  swiperSlidesConfig: SwiperOptions = {
    slidesPerView: 1,
    spaceBetween: 0,
    navigation: true,
    pagination: { clickable: true },
  };

  carousels: Array<any> = [];

  videoFocusOut = false;
  carousalValue: string = "";

  @HostListener('window:scroll', ['$event'])
  track(event: any) {
    if (window.pageYOffset > 400) {
      this.videoFocusOut = true;
    } else {
      this.videoFocusOut = false;
    }
  }

  ngOnInit(): void {
    this.getVideoCarousal();
  }

  getVideoCarousal() {
    this.carousalValue = this.videoctrl.getVideoType();
    console.log(this.carousalValue);
   var url = APIS.ADMIN_VIDEO_CAROUSAL( this.carousalValue);
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("ADMIN_VIDEO_CAROUSAL==>", res);
      if (res.statusCode == 200) {
        this.carousels = res.data;
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }


}
