import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CommonService } from 'src/app/providers/common.service';
import { Router } from '@angular/router';
import { SearchComponent } from '../search/search.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';

@Component({
  selector: 'app-home-navbar',
  templateUrl: './home-navbar.component.html',
  styleUrls: ['./home-navbar.component.scss']
})
export class HomeNavbarComponent implements OnInit {
  scrolled: boolean = false;
  navbarCollapse: boolean = false;

  toggleNavbar() {
    this.navbarCollapse = !this.navbarCollapse;
  }

  userInfo: any;
  genres: any = [];
  profile_pic: string = "assets/imgs/icons/user.png";

  @HostListener("window:scroll", [])
  onWindowScroll() {
    this.scrolled = window.scrollY > 100;
  }

  menus = [
    { link: '/home', label: 'Home' },
    { link: '/home/movies', label: 'Movies' },
    { link: '/home/webseries', label: 'Web Series' },
    { link: '/home/tvshows', label: 'TV Shows' },
    { link: '/home/live', label: 'Live' },
    // { link: '/home/newandpopular', label: 'New & Popular' },
  ]

  constructor(@Inject(DOCUMENT) private document: Document,
    public commonservice: CommonService,
    public router: Router,
    private modalService: NgbModal,) {
    this.userInfo = this.commonservice.getuserInfo();
    console.log(this.userInfo);

    this.commonservice.changeProfle.subscribe(res => {
      if (res) {
        this.ngOnInit();
      }
    })
  }

  ngOnInit(): void {
    this.userInfo = this.commonservice.getuserInfo();

    if (this.userInfo.userDP) {
      this.profile_pic = this.userInfo.userDP;
    }

    this.getGenres();

  }

  searchModal() {
    const modalRef = this.modalService.open(SearchComponent, {
      windowClass: 'modal-search',
      backdropClass: 'darken',
      // centered:true
    });

    // this.videocontrolservice.pauseVideo();
  }

  getGenres() {
    this.commonservice.getMethod(APIS.MASTER_GET_ALL_GENRES).subscribe(
      (res: any) => {
        console.log(res);
        this.genres = res.data;
      })
  }

  openGenre(genre: any) {
    this.router.navigate(['/home/genre/', genre.id]);
  }

  logout() {
    this.commonservice.logout();
    this.router.navigate(['/']);
  }

}
