import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal) { }

  
  ngOnInit(): void {
  }
  items =[
    {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-1.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4'
    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-2.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'
    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-3.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-4.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-5.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-2.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-3.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'
    },
    {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-5.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }, {
      title: "Movie Name",
      description: "Eight year-old Spider man No way Home learns that 786 is God’s number and decides to try and reach out to God, by dialing this number.",
      img: './assets/imgs/thumb-movie-2.png',
      videourl: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4'

    }
  ]

}
