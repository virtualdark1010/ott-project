
import { NgModule, CUSTOM_ELEMENTS_SCHEMA ,} from '@angular/core';
import { OnlyNumberDirective } from './only-number.directive';

@NgModule({
    declarations: [
        OnlyNumberDirective,
    ],
    imports: [
    ],
    exports:[
        OnlyNumberDirective,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DirectiveModule { }
