import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAuthGuard, AdminAuthGuard } from './providers/guards/auth.guard';

const routes: Routes = [
  { path: "", redirectTo: '/', pathMatch: 'full' },
  {
    path: "",
    loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule),
  },
  {
    path: "home",
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    // canActivate: [UserAuthGuard]
  },
  {
    path: "admin",
    loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // onSameUrlNavigation: 'reload',
    // scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
