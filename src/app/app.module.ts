import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DefaultModule } from './default/default.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//providers
import { CommonService } from './providers/common.service';
import { EncrDecrService } from './providers/encr-decr.service';
import { IntercepterService } from './providers/intercepter.service';
import { AdminAuthGuard, UserAuthGuard,  } from './providers/guards/auth.guard';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { VideoControlsService } from './providers/video-controls.service';
import { ModalService } from './providers/modal.service';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subscriptor } from './providers/subscriptor';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    DefaultModule
  ],
  providers: [
    CommonService,
    EncrDecrService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: IntercepterService,
      multi: true
    },
    VideoControlsService,
    ModalService,
    NgbActiveModal,
    Subscriptor,

    UserAuthGuard

    // ScreenTrackingService,
    // UserTrackingService,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],

})
export class AppModule { }
