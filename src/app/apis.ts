import { environment } from "src/environments/environment";


const makeUrl = (url: string, key: string, value: string) => {
    return url.includes('?') ? `${url}&${key}=${value}` : `${url}?${key}=${value}`;
}


export const APIS = {

    // user master controller
    USER_REGISTER_OR_UPDATE: "auth/signup",
    USER_LOGIN: "auth/signin",
    USER_LOGIN_WITH_MOBILE: "user/loginWithMobile",
    GET_All_USERS: "user/getAll",
    DELETE_USER: (userId: string) => {  //post
        return `user/delete?userId=${userId}`;
    },
    VERIFY_MOBILE_NUMBER: "user/verifyMobileNumber",


    GET_USER_DETAILS_BY_ID: (userId:string)=>{
        return `user/getUserDetails?userId=${userId}`
    },
    UPLOAD_PROFILE_PIC: (userId:string)=>{
        return environment.baseUrl + `admin/source/uploadProfileDP?userId=${userId}`
        
    },
    // UPDATE_USER_PASSWORD:  "user/updatepassword",

    // Admin
    ADD_ADMIN: "auth/addAdmin",


    //user role mapping controller
    ROLE_MAPPING_SAVE_OR_UPDATE: "rolemapping/saveOrUpdate",
    ROLE_MAPPING_DELETE: "rolemapping/delete",
    GET_ROLE_MAPPING: "rolemapping/get",
    GET_ROLE_MAPPING_USER_ROLES: "rolemapping/getUserRoles",
    GET_ALL_ROLE_MAPPINGS: "rolemapping/getAll",

    //role master controller
    ROLE_SAVE_OR_UPDATE: "role/saveOrUpdate",
    ROLE_DELETE: "role/delete",
    GET_ROLE: "role/get",
    GET_ALL_ROLES: "role/getAll",



    // videos
    ADMIN_VIDEO_SAVE_OR_UPDATE: "admin/video/saveOrUpdate",
    ADMIN_GET_ALL_VIDEOS: "admin/video/getAll",
    ADMIN_GET_VIDEO_DETAILS: (id: string) => {  //GET
        return `admin/video/getVideoDetails?id=${id}`;
    },
    // ADMIN_GET_VIDEO_DETAILS_BY_GENRE: (genre: string) => {  //GET
    //     return `admin/video/getVideoDetailsByGenre?genre=${genre}`;
    // },

    ADMIN_GET_VIDEO_DETAILS_BY_GENRE: 'admin/video/getVideoDetailsByGenre',  

    ADMIN_GET_VIDEO_URLS: (videoId: string) => {  //GET
        return `admin/language/get?videoId=${videoId}`;
    },
    ADMIN_VIDEO_CAST_SAVE_OR_UPDATE: "admin/videoCast/saveOrUpdate",
    ADMIN_DELETE_VIDEO_BY_ID: (id: string) => {  //GET
        return `admin/video/deleteVideo?videoId=${id}`;
    },

    GET_TOP_TEN_VIDEOS: (videoType: string) => {
        if (videoType) {
            return `admin/video/topTenVideos?videoType=${videoType}`;
        } else {
            return "admin/video/topTenVideos"
        }
    },

    ADMIN_GET_RECOMMENDED_VIDEOS: (videoId: string, videoType: string) => {
        return `admin/video/getRecommendedVideos?videoId=${videoId}&videoType=${videoType}`;
    },//GET


    //master
    MASTER_GET_ALL_GENRES: "master/getAllGenres", //GET
    GET_COUNTRY_BY_ID: (ip: string) => {  //GET
        return 'master/getCountrybyIP?ipAddress=' + ip;
    },
    MASTER_GET_VIDEO_TYPES: "master/getVideoTypes", //GET
    MASTER_GET_ALL_COUNTRIES: "master/getAllCountries", //GET
    MASTER_GET_ALL_LANGUAGES: "master/getAllLanguages", //GET


    //cast
    ADMIN_GET_ALL_CAST_DETAILS: "admin/cast/getAllCastDetails", //GET
    ADMIN_CAST_SAVE_OR_UPDATE: "admin/cast/saveOrUpdate", //POST
    ADMIN_CAST_DELETE: (id: string) => {
        return `admin/cast/delete?id=${id}`;
    },//Post
    ADMIN_GET_CAST_DETAIL_LIST_BY_IDS: (ids: string) => {  //GET
        return `admin/cast/getCastDetailList?ids=${ids}`;
    },

    ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID: (id: string) => {  //GET
        return `admin/videoCast/getDetails?videoId=${id}`;
    },

    SEARCH_GET_CAST_INFO_BY_NAME: (name: string) => {  //GET
        return `search/getCastInfo?value=${name}`;
    },


    //LANGUAGE
    ADMIN_LANGUAGE_SAVE_OR_UPDATE: "admin/language/saveOrUpdate", //POST
    // ADMIN_GET_ALL_LANGUAGES: "admin/language/getAllLanguages", //GET
    ADMIN_DELETE_LANGUAGE: "admin/language/delete", //POST

    //upload source
    ADMIN_UPLOAD_FILE: environment.baseUrl + "admin/source/uploadFile", //POST
    // ADMIN_UPLOAD_FILE: "admin/source/uploadFile", //POST
    ADMIN_EPISODE_UPLOAD: environment.baseUrl + "admin/video/episodes/uploadEpisode", //POST


    ADMIN_UPLOAD_PROFILE_DP: environment.baseUrl + "admin/source/uploadProfileDP", //POST
    ADMIN_UPLOAD_ARTIST_DP: (castId: string) => {
        return environment.baseUrl + `admin/source/uploadArtistDP?castId=${castId}`  //POST
    },

    //Genre
    ADMIN_GENRE: "master/getAllGenres", //Get call
    ADMIN_GENRE_SAVE_OR_UPDATE: "admin/genre/saveOrUpdate", //Post
    ADMIN_GENRE_DELETE: "admin/genre/delete", //Post

    //homepage Carousal
    ADMIN_VIDEO_CAROUSAL: (videoType: string) => {
        if (videoType)
            return `admin/video/getVideoCarousal?videoType=${videoType}`;
        else
            return "admin/video/getVideoCarousal"
    },

    //save video preference
    VIDEO_PREFERENCE: "user/saveVideoPreference",
    GET_VIDEO_PREFERENCE: (username: string) => {
        return `user/getVideoPreference?userName=${username}`;
    },

    THREEMONTHS: "user/lastThreeMonthUserSubscriptions",
    SIXMONTHS: "user/lastSixMonthUserSubscriptions",
    ONEYEAR: "user/lastOneYearUserSubscriptions",
    ONEMONTH: "user/lastOneMonthUserSubscriptions",
    ALL: "user/getAllSubscriptionDetails",

    LATEST_AND_TRENDING: (videoType: string) => {
        if (videoType) {
            return `admin/video/getLatestAndTrending?videoType=${videoType}`;
        } else {
            return `admin/video/getLatestAndTrending`
        }
    },

    //seasons
    ADMIN_SEASON_SAVE_OR_UPDATE: "admin/video/season/saveOrUpdate", //POST
    GET_SEASION_DETAILS: (seasionId: string) => {
        return `admin/video/season/getSeasonDetails?id=${seasionId}`;
    },
    GET_SEASONS_BY_VIDEOID: (videoId: string) => {
        return `admin/video/season/getAllByVideoId?videoId=${videoId}`;
    },

    //episodes 
    ADMIN_EPISODE_SAVE_OR_UPDATE: "admin/video/episodes/saveOrUpdate", //POST
    UPDATE_EPISODE_URL: (episodeId: string, url: string) => {
        return `admin/video/episodes/updateEpisodeURl?episodeId=${episodeId}&url=${url}`
    },
    GET_EPISODE_DETAILS: (episodeid: string) => {
        return `admin/video/episodes/getEpisodeDetails?id=${episodeid}`;
    },
    GET_ALL_EPISODES_BY_SEASION_ID: (seasonId: string | null) => {
        return `admin/video/episodes/getAllBySeasonId?seasonId=${seasonId}`;
    },


    //continue watching
    GET_CONTINUE_WATCHING: (username: string) => {
        return `user/getRecentlyWatchedMovies?username=${username}`;
    },
    SAVE_CONTINUE_WATCHING: "user/saveWatchedDuration",


    //sections
    ADMIN_SECTION_SAVE_OR_UPDATE: "admin/sections/saveOrUpdate", //POST
    ADMIN_SECTION_DELETE_BY_ID: (id: string) => {
        return `admin/sections/delete?id=${id}`
    },
    ADMIN_GET_ALL_SECTIONS: "admin/sections/getAll", //GET
    GET_ALL_SECTIONS_BY_VIDEO_TYPE: (videoType: string) => {
        return `admin/sections/getSectionByVideoType?videoType=${videoType}`;
    }


}