import { Component, OnInit, TemplateRef } from '@angular/core';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'ott-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.scss'],
  host: {'class': 'toast-container position-fixed top-0 end-0 p-3', 'style': 'z-index: 1200'}
})
export class ToasterComponent implements OnInit {

  constructor(public commonservice: CommonService) {}

  ngOnInit(): void {
  }

  isTemplate(toast:any) { return toast.textOrTpl instanceof TemplateRef; }

}
