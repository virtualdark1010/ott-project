import { Injectable } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ExploreAllComponent } from '../landing/exploreAll/explore-all.component';
import { ModalForgotPasswordComponent } from '../landing/modal-forgot-password/modal-forgot-password.component';
import { ModalLoginComponent } from '../landing/modal-login/modal-login.component';
import { ModalSignupComponent } from '../landing/modal-signup/modal-signup.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal) { }

  openSignupModal() {
    this.closeModal();
    this.modalService.open(ModalSignupComponent, {
      size: 'lg',
      windowClass: 'modal-auth',
      backdropClass: 'darken',
      centered: true
    });
  }
  openExploreAll(title: string, videos: any) {
    const explore = this.modalService.open(ExploreAllComponent, {
      windowClass: 'modal-auth',
      backdropClass: 'darken',
      centered: true
    });
    explore.componentInstance.data = videos;
    explore.componentInstance.title = title;
  }

  openSigninModal() {
    this.closeModal();
    this.modalService.open(ModalLoginComponent, {
      size: 'md',
      windowClass: 'modal-auth',
      backdropClass: 'darken',
      centered: true
    });
  }

  forgotPasswordModal() {
    this.closeModal();
    this.modalService.open(ModalForgotPasswordComponent, {
      windowClass: 'modal-auth',
      backdropClass: 'darken',
      centered: true
    });
  }

  closeModal(msg = 'close') {
    this.modalService.dismissAll(msg);
  }

  

}
