import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router } from '@angular/router';
import { CommonService } from '../common.service'

@Injectable({
  providedIn: 'root'
})
export class UserAuthGuard implements CanActivate {
  constructor(private commonService: CommonService, private router: Router) { };
  canActivate(): boolean {
    let userInfo = this.commonService.getuserInfo();
    if (this.commonService.isLoggedIn() && (userInfo.roles.includes('ROLE_USER'))) {
      return true;
    } else {
      this.router.navigate(['/']);
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root'
})
export class AdminAuthGuard implements CanActivateChild {

  constructor(private commonService: CommonService, private router: Router) { };

  canActivateChild(): boolean {
    let userInfo = this.commonService.getuserInfo();
    if (this.commonService.isLoggedIn() && userInfo.roles.includes('ROLE_ADMIN')) {
      return true;
    } else {
      this.router.navigate(['/admin/login']);
      return false;
    }
  }
}
