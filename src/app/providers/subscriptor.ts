import { Subscription } from "rxjs";

export class Subscriptor {
  public static subscriptions: Subscription[] = [];

  static set subscription(subscription: Subscription) {
    Subscriptor.subscriptions.push(subscription);
  }

  static unsubscribe() {
    Subscriptor.subscriptions.forEach(subscription => subscription ? subscription.unsubscribe() : 0);
    Subscriptor.subscriptions = [];
  }
}