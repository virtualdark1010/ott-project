import { TestBed } from '@angular/core/testing';

import { VideoControlsService } from './video-controls.service';

describe('VideoControlsService', () => {
  let service: VideoControlsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoControlsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
