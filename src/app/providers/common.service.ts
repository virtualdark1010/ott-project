import { Injectable, NgZone, TemplateRef } from '@angular/core';
import { HttpClient, HttpProgressEvent } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { EncrDecrService } from './encr-decr.service'

import { environment } from 'src/environments/environment';
var baseUrl = environment.baseUrl;

@Injectable({
  providedIn: 'root'
})

export class CommonService {

  paidUser$ = new BehaviorSubject<Boolean>(false);
  getPaidUsers$ = this.paidUser$.asObservable();

  toasts: any[] = [];

  profilePicUpdated$ : BehaviorSubject<any> = new BehaviorSubject(null);
  changeProfle = this.profilePicUpdated$.asObservable();
  constructor(private http: HttpClient,
    private EncrDecrService: EncrDecrService,
    private zone: NgZone,
  ) { }

  getMethod(url: string): Observable<any> {
    return this.http.get(baseUrl + url).pipe()
  }

  postMethod(url: string, data: any): Observable<any> {
    return this.http.post(baseUrl + url, data).pipe()
  }

  deleteMethod(url: string): Observable<any> {
    return this.http.delete(baseUrl + url).pipe()
  }


  basicFileUpload(url: string, files: FileList) {
    var formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f))
    return this.http.post(baseUrl + url, formData)
  }

  uploadFileWithProgress(url: string, formData: FormData) {
    debugger
    return this.http.post(baseUrl + url, formData, { reportProgress: true, observe: 'events' }).pipe();
  }

  calcProgressPercent(event: HttpProgressEvent | any): number {
    return Math.round(100 * event.loaded / event.total);
  }



  isLoggedIn() {
    return !!localStorage.getItem('userInfo');
  }

  setuserInfo(user: any) {
    let userInfo = this.EncrDecrService.set(user);
    localStorage.setItem('userInfo', userInfo);
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  logout() {
    localStorage.clear();
  }

  getuserInfo() {
    if (this.isLoggedIn()) {
      const userDetails = this.EncrDecrService.get(localStorage.getItem('userInfo'));
      return userDetails;
    } else {
      return null;
    }
  }

  public snackbar(message: string, action = 'success' || 'error' || 'warning', duration = 4000): void {
    this.zone.run(() => {
      switch (action) {
        case 'success': {
          this.showToast(message, { classname: 'bg-success text-light', delay: duration });
          break;
        }
        case 'error': {
          this.showToast(message, { classname: 'bg-danger text-light', delay: duration });
          break;
        }
        case 'warning': {
          this.showToast(message, { classname: 'bg-warning text-light', delay: duration });
          break;
        }
      }
    });
  }

  showToast(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  removeToast(toast: any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }


  getIPAddress(): Observable<any> {
    return this.http.get<{ ip: string }>('https://jsonip.com').pipe();
  }

  getVideoLength(time: string): number {
    let totalSec = 0;
    if (time) {
      let timeArr = time.split(':');
      let hours = parseInt(timeArr[0]);
      let min = parseInt(timeArr[1]);
      let sec = parseInt(timeArr[2]);

      totalSec = Math.floor(hours * 60 * 60) + Math.floor(min * 60) + sec;
    }
    return totalSec;
  }

  async getCompletedPercentage(movieLength: string, watchDuration: string) {
    let completedPercentage = 0;
    if (movieLength && watchDuration) {
      let totalSec = await this.getVideoLength(movieLength);
      console.log("totalSec", totalSec);
      completedPercentage = (100 * parseInt(watchDuration)) / totalSec;

      console.log("completedPercentage", completedPercentage);
    }
    return Math.ceil(completedPercentage);
  }

  getVideoLengthHrsToMin(time: string): number {
    let totalMin = 0;
    if (time) {
      let timeArr = time.split(':');
      let hours = parseInt(timeArr[0]);
      let min = parseInt(timeArr[1]);

      totalMin = Math.floor(hours * 60) + Math.floor(min);
    }
    return totalMin;
  }

  getVideoLengthSecToMin(seconds: number = 0): number {
    let totalMin = Math.floor(seconds / 60);
    return totalMin;
  }

  titlecase(value: string): string {
    let first = value.substr(0, 1).toUpperCase();
    return first + value.substr(1);
  }


  changeProfile(updated: boolean = false){
    this.profilePicUpdated$.next(updated)
  }

}
