import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { APIS } from '../apis';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class VideoControlsService {

  videoController$ = new BehaviorSubject<any>(null);

  constructor(public commonservice: CommonService,
    public router: Router) { }

  playVideo() {
    this.videoController$.next({
      action: 'play'
    });
  }

  pauseVideo() {
    this.videoController$.next({
      action: 'pause'
    });
  }


  getStringFormatDate(date = { year: 0, month: 0, day: 0 }): string {
    let year = date.year;
    let month = date.month <= 9 ? '0' + date.month : date.month;
    let day = date.day <= 9 ? '0' + date.day : date.day;
    let finalDate = year + "-" + month + "-" + day;
    return finalDate;
  }


  async getVideosByGenre(genre: Array<string>): Promise<Observable<any>> {
    const videos = await new Promise(async (resolve, reject) => {
      this.commonservice.postMethod(APIS.ADMIN_GET_VIDEO_DETAILS_BY_GENRE, { genreList: genre }).subscribe((res: any) => {
        if (res.statusCode == 200) {
          resolve(res.data || []);
        } else {
          this.commonservice.snackbar(res.message, 'error')
          resolve([]);
        }
      }, err => {
        this.commonservice.snackbar(err.message, 'error')
        resolve([]);
        // {title:'siva',type:'sivakrishna',image:'siva',video:'siva'}
      })
    });
    return of(videos);
  }

  async getVideosAndDetailsByGenre(genre: Array<string>): Promise<Observable<any>> {
    const videos = await new Promise(async (resolve, reject) => {
      this.commonservice.postMethod(APIS.ADMIN_GET_VIDEO_DETAILS_BY_GENRE, { genreList: genre }).subscribe((res: any) => {
        if (res.statusCode == 200) {
          resolve(res.data || []);
        } else {
          this.commonservice.snackbar(res.message, 'error')
          resolve([]);
        }
      }, err => {
        this.commonservice.snackbar(err.message, 'error')
        resolve([]);
        // {title:'siva',type:'sivakrishna',image:'siva',video:'siva'}
      })
    });
    return of(videos);
  }



  getVideoType() {
    let type = this.router.url.split('/')[2] || this.router.url.split('/')[1];
    let value = '';
    switch (type) {
      case 'home':
        value = "";
        break;
      case 'newandpopular':
        value = ""
        break;
      case 'movies':
        value = "movie";
        break;
      case 'webseries':
        value = "web series"
        break;
      case 'tvshows':
        value = "tv shows"
        break;
      default:
        value = '';
        break;
    }
    return value;
  }

  getSectionVideoType() {
    let type = this.router.url.split('/')[2] || this.router.url.split('/')[1];
    let value = '';
    switch (type) {
      case 'home':
        value = "home";
        break;
      case 'newandpopular':
        value = ""
        break;
      case 'movies':
        value = "movie";
        break;
      case 'webseries':
        value = "web series"
        break;
      case 'tvshows':
        value = "tv shows"
        break;
      default:
        value = '';
        break;
    }
    return value;
  }



}
