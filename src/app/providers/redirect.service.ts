import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EncrDecrService } from './encr-decr.service'

@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  constructor(public router: Router, private encrDecrService: EncrDecrService) { }

  // home module
  watchNow(videoId: string, params?: any) {
    if (videoId) {
      if (params) {
        let query_params = this.encrDecrService.set(JSON.stringify(params))
        this.router.navigate([`/home/watchnow/${videoId}`], { queryParams: { data: query_params } });
      } else {
        this.router.navigate([`/home/watchnow/${videoId}`]);
      }
    }
  }

  preview(videoId: string | null) {
    if (videoId) {
      this.router.navigate([`/home/preview/${videoId}`]);
    }
  }


  // admin module
  createVideo() {
    this.router.navigate(['/admin/video/create']);
  }
  updateVideo(videoId: string) {
    this.router.navigate([`/admin/video/update/${videoId}`]);
  }
  privacyAndRestriction(videoId: string) {
    this.router.navigate([`/admin/video/privacy/${videoId}`]);
  }
  uploadVideos(videoId: string) {
    this.router.navigate([`/admin/video/upload/${videoId}`]);
  }
  castAndCrew(videoId: string) {
    this.router.navigate([`/admin/video/cast-crew/${videoId}`]);
  }
  payment(videoId: string) {
    this.router.navigate([`/admin/video/payment/${videoId}`]);
  }
  createNewSeason(videoId: string) {
    this.router.navigate([`/admin/video/seasons/${videoId}`]);
  }
  editSeason(videoId: string, seasonId: string) {
    this.router.navigate([`/admin/video/seasons/${videoId}/${seasonId}`]);
  }

  episodes(seasonId: string) {
    this.router.navigate([`/admin/video/episodes/${seasonId}`]);
  }

  editEpisode(seasonId: string, episodeId: string) {
    this.router.navigate([`/admin/video/episodes/${seasonId}/${episodeId}`]);
  }

}
