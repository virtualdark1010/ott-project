import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'kismat-type-cast',
  templateUrl: './type-cast.component.html',
  styleUrls: ['./type-cast.component.scss']
})
export class TypeCastComponent implements OnInit {
  public typeCastForm!: FormGroup;
  submitted: boolean = false;
  
  constructor() { }

  ngOnInit(): void {
    this.typeCastForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      profileDP: new FormControl("", [Validators.required]),
      dob: new FormControl("", [Validators.required]),
      castedIn: new FormControl("", [Validators.required]),
      description: new FormControl("", [Validators.required]),
    })
  }
  typeCast(){}

}
