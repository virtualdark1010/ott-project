import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeCastComponent } from './type-cast.component';

describe('TypeCastComponent', () => {
  let component: TypeCastComponent;
  let fixture: ComponentFixture<TypeCastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeCastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeCastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
