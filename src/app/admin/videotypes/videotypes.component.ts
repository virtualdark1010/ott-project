import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-videoTypes',
  templateUrl: './videotypes.component.html',
  styleUrls: ['./videotypes.component.scss']
})
export class VideoTypesComponent implements OnInit {
  public videoForm = new FormGroup({
    id: new FormControl(''),
    videoType: new FormControl("", [Validators.required]),
  });
  submitted: boolean = false;
  videoType: any = [];
  videoHeaders = [
    { field: 'id', lable: 'S.No', type: 'text' },
    { field: 'videoType', lable: 'Video', type: 'text' }
  ];

  editMode: boolean = false

  constructor(public commonservice: CommonService) { }

  ngOnInit(): void {
  }



  onSelectItemForEdit(item: any) {
    console.log("item==>", item);
  }

  onSelectItemForDelete(item: any) {
    console.log("item==>", item);
  }

}
