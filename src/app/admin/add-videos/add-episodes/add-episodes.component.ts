import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

@Component({
  selector: 'app-add-episodes',
  templateUrl: './add-episodes.component.html',
  styleUrls: ['./add-episodes.component.scss']
})
export class AddEpisodesComponent implements OnInit {

  @Output() onCreated = new EventEmitter<any>();

  seasonId: any;
  episodeId: any;

  episode: any;

  payload = this.fb.group({
    seasonId: [null],
    episodeId: [null],
    title: ['', [Validators.required]],
    subTitle: ['', [Validators.required]],
    releaseDate: [null, [Validators.required]],
    description: ['', [Validators.required]],
    movieLength: ['', [Validators.required]],
    // episodeUrl: [null, [Validators.required]],
  })

  isEditMode: boolean = false;
  submitted: boolean = false;



  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public videocontrolservice: VideoControlsService,
    public redirectservice: RedirectService,
    public activateRoute: ActivatedRoute,
    public router: Router) {
    activateRoute.params.subscribe(param => {
      if (param.seasonid) {
        this.seasonId = param.seasonid;
        this.episodeId = param?.episodeid || "";

        if (this.episodeId) {
          this.isEditMode = true;
          this.getEpisodeDetails();
        }
      } else {
        this.isEditMode = false;
      }
    })

  }

  ngOnInit(): void {

  }


  getEpisodeDetails() {
    this.commonservice.getMethod(APIS.GET_EPISODE_DETAILS(this.episodeId))
      .subscribe((res: any) => {
        console.log("GET_EPISODE_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.episode = res.data;
          this.payload.patchValue(this.episode);
          console.log("this.payload", this.payload);  
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }

  onDateSelect(event: any) {
    let releasedate = this.videocontrolservice.getStringFormatDate(event);
    this.payload.patchValue({ releaseDate: releasedate });
  }

  cancel() {
    this.redirectservice.episodes(this.seasonId);
  }

  episodes() {
    this.redirectservice.episodes(this.seasonId);
  }

  createEpisode() {
    // activeModal.dismiss('Cross click');

    // console.log("videoForm", this.seasonForm);

    // return
    this.payload.controls.seasonId.setValue(this.seasonId);
    if (this.episodeId) {
      this.payload.controls.episodeId.setValue(this.episodeId);
    }

    this.submitted = true;

    if (!this.payload.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }
    var url = APIS.ADMIN_EPISODE_SAVE_OR_UPDATE;
    let episode = this.payload.getRawValue();
    debugger
    this.commonservice.postMethod(url, episode)
      .subscribe((res: any) => {
        console.log("ADMIN_EPISODE_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.payload.reset();
          this.commonservice.snackbar(this.isEditMode ? "Episode Updated Successfully" : "Episode Created Successfully");
          // this.redirectservice.privacyAndRestriction(res.data.id);
          if (this.isEditMode) {
            this.redirectservice.episodes(this.seasonId);
          }
          this.submitted = false;

          this.onCreated.emit('done')
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })

  }


}
