import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subscriptor } from 'src/app/providers/subscriptor'
import { RedirectService } from 'src/app/providers/redirect.service';

@Component({
  selector: 'kismat-cast-and-crew',
  templateUrl: './cast-and-crew.component.html',
  styleUrls: ['./cast-and-crew.component.scss']
})
export class CastAndCrewComponent implements OnInit {
  videoId: any;
  video: any;
  searchActorByName: FormControl = new FormControl('');
  actors: any = [];
  videoActors: any = [];

  addCast = this.fb.group({
    videoId: [''],
    castId: ['', [Validators.required]],
    designation: ['', [Validators.required]],
  })
  submitted: boolean = false;
  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public activateRoute: ActivatedRoute,
    public redirectservice: RedirectService
  ) {
    this.activateRoute.params.subscribe(params => {
      this.videoId = params.videoid
    })
  }

  ngOnInit(): void {
    this.getActorsByVideoId();
    this.getVideoDetails();
   
    this.searchActorByName.valueChanges.subscribe((val: string) => {
      console.log("val==>", val);
      Subscriptor.unsubscribe();
      if (val && typeof (val) == 'string') {
        this.getActors();
      }
    })
  }

  getVideoDetails() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_VIDEO_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.video = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }
  getActors() {
    Subscriptor.subscription = this.commonservice.getMethod(
      APIS.SEARCH_GET_CAST_INFO_BY_NAME(this.searchActorByName.value))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_ALL_CAST_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.actors = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  // public model: any;
  filteredActors: OperatorFunction<string, readonly { castName: any, id: any }[]> = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.actors.filter((v: any) => v.castName.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );
  formatter = (x: { castName: string }) => x.castName;


  getActorsByVideoId() {
    this.commonservice.getMethod(APIS.ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID==>", res);
        if (res.statusCode == 200) {
          this.videoActors = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  selectActor(event: any) {
    console.log("event==>", event);
    this.addCast.patchValue({
      castId: event.item.id
    })
    this.searchActorByName.setValue('');
  }

  addActor() {
    // activeModal.dismiss('Cross click')
    console.log("addCast", this.addCast);
    this.submitted = true;

    this.addCast.patchValue({ videoId: this.videoId });

    if (!this.addCast.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }
    var url = APIS.ADMIN_VIDEO_CAST_SAVE_OR_UPDATE;
    let user = this.addCast.getRawValue();
    this.commonservice.postMethod(url, user)
      .subscribe((res: any) => {
        console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.commonservice.snackbar("Successfully");
          this.getActorsByVideoId();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }


  save() {
    let reqs = {
      id: this.videoId,
      saved: true,
      publish: false
    };
    this.saveOrPublish(reqs);
  }

  publish() {
    let reqs = {
      id: this.videoId,
      saved: false,
      publish: true
    };
    this.saveOrPublish(reqs);
  }

  saveOrPublish(reqs: any = {}) {
    this.commonservice.postMethod(APIS.ADMIN_VIDEO_SAVE_OR_UPDATE, reqs).subscribe((res: any) => {
      console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
      if (res.statusCode == 200) {
        this.commonservice.snackbar(reqs.saved? "Content Saved Successfully" : "Content Published Successfully");
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }


}
