import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { languages } from 'src/app/shared/video-statics';

@Component({
  selector: 'kismat-create-video',
  templateUrl: './create-video.component.html',
  styleUrls: ['./create-video.component.scss']
})
export class CreateVideoComponent implements OnInit {
  types: any = [];
  languages: any = languages;
  video: any;
  videoId: any;
  videoForm = this.fb.group({
    id: [null],
    videoType: ['', [Validators.required]],
    title: ['', [Validators.required]],
    subTitle: [''],
    // season: [null],
    // episode: [null],
    releaseDate: [null, [Validators.required]],
    ottReleaseDate: [null, [Validators.required]],
    description: ['', [Validators.required]],
    audioLanguage: ['', [Validators.required]],
  })

  isEditMode: boolean = false;
  submitted: boolean = false;
  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public videocontrolservice: VideoControlsService,
    public redirectservice: RedirectService,
    public activateRoute: ActivatedRoute) {
    // this.getLanguages(); 

    activateRoute.params.subscribe(param => {
      if (param.videoid) {
        this.isEditMode = true;
        this.videoId = param.videoid;
        this.getVideoDetails();
      } else {
        this.isEditMode = false;
      }
    })

  }

  ngOnInit(): void {
    this.getVideoTypes();
    this.getLanguages();
  }


  getVideoDetails() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_VIDEO_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.video = res.data;
          this.videoForm.patchValue(this.video);
          console.log("this.videoForm", this.videoForm);
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }

  getVideoTypes() {
    this.commonservice.getMethod(APIS.MASTER_GET_VIDEO_TYPES).subscribe((res: any) => {
      console.log("MASTER_GET_VIDEO_TYPES==>", res);
      if (res.statusCode == 200) {
        this.types = res.data;
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    })
  }

  getLanguages() {
    this.commonservice.getMethod(APIS.MASTER_GET_ALL_LANGUAGES)
      .subscribe((res: any) => {
        console.log("MASTER_GET_ALL_LANGUAGES==>", res);
        if (res.statusCode == 200) {
          this.languages = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  onDateSelect(event: any, fControl: string) {
    let releasedate = this.videocontrolservice.getStringFormatDate(event);
    let patchValues: any = {};
    patchValues[fControl] = releasedate;
    this.videoForm.patchValue(patchValues);
  }

  createVideo() {
    console.log("videoForm", this.videoForm);
    this.submitted = true;
    if (!this.videoForm.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }
    var url = APIS.ADMIN_VIDEO_SAVE_OR_UPDATE;
    let user = this.videoForm.getRawValue();
    this.commonservice.postMethod(url, user)
      .subscribe((res: any) => {
        console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.videoForm.reset();
          this.commonservice.snackbar(this.isEditMode ? "Video Updated Successfully" : "Video Created Successfully");
          this.redirectservice.privacyAndRestriction(res.data.id);
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })

  }


}
