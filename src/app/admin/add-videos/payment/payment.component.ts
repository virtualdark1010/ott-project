import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { Observable, OperatorFunction } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Subscriptor } from 'src/app/providers/subscriptor'
import { RedirectService } from 'src/app/providers/redirect.service';

@Component({
  selector: 'kismat-paymeny',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  videoId: any;
  video: any;

  paymentOption = this.fb.group({
    payment: ['', [Validators.required]],
    amount: [100],
    currency: [''],
  });


  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public activateRoute: ActivatedRoute,
    public redirectservice: RedirectService
  ) {
    this.activateRoute.params.subscribe(params => {
      this.videoId = params.videoid
    })
  }

  ngOnInit(): void {
    this.getVideoDetails();
  }

  getVideoDetails() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_VIDEO_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.video = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }


  paymentTypeChange() {
    this.paymentOption.get('amount')?.setValue("");
    this.paymentOption.get('currency')?.setValue("");
  }

  save() {
    let reqs = {
      id: this.videoId,
      publish: false,
      paymentOption: this.paymentOption.getRawValue()
    };
    this.saveOrPublish(reqs);
  }

  publish() {
    let reqs = {
      id: this.videoId,
      publish: true,
      paymentOption: this.paymentOption.getRawValue(),
      videoFinanceType: "PAY"
    };
    this.saveOrPublish(reqs);
  }

  saveOrPublish(reqs: any = {}) {

    this.commonservice.postMethod(APIS.ADMIN_VIDEO_SAVE_OR_UPDATE, reqs).subscribe((res: any) => {
      console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
      if (res.statusCode == 200) {
        this.commonservice.snackbar(reqs.saved ? "Content Saved Successfully" : "Content Published Successfully");
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }


}
