import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { uploadingTypes } from 'src/app/shared/video-statics';
import { HttpClient,  HttpEventType } from '@angular/common/http';

@Component({
  selector: 'kismat-upload-video',
  templateUrl: './upload-video.component.html',
  styleUrls: ['./upload-video.component.scss']
})
export class UploadVideoComponent implements OnInit {
  videoId: any;
  video: any;
  uploadingType: string = "";

  public uploadingTypes = uploadingTypes;

  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public http: HttpClient,
    public activateRoute: ActivatedRoute,
    public redirectservice: RedirectService) {
    this.activateRoute.params.subscribe(params => {
      this.videoId = params.videoid;

      this.getVideoDetails();
    })
  }

  ngOnInit(): void {
  }


  getVideoDetails() {
    this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_VIDEO_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.video = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }

  fileUploaded(event:any) {
    this.getVideoDetails();
  }
  


}
