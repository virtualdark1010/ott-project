import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrivacyVideoComponent } from './privacy-video.component';

describe('PrivacyVideoComponent', () => {
  let component: PrivacyVideoComponent;
  let fixture: ComponentFixture<PrivacyVideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrivacyVideoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivacyVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
