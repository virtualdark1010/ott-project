import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { uploadingTypes } from 'src/app/shared/video-statics';

@Component({
  selector: 'app-privacy-video',
  templateUrl: './privacy-video.component.html',
  styleUrls: ['./privacy-video.component.scss']
})
export class PrivacyVideoComponent implements OnInit {
  genres: any = [];
  backupGenres: any = [];
  visibility: any = [];
  countries: any = [];

  videoId: any;
  video: any;

  public videoForm = this.fb.group({
    id: ['', [Validators.required]],
    countryOrigin: ['', [Validators.required]],
    contentCertificate: ['U/A', [Validators.required]],
    rating: ['', [Validators.required]],
    movieLength: ['', [Validators.required]],
    visibility: this.fb.group({
      exclude: [[]],
      include: [[], [Validators.required]],
    }),
    genres: [[], [Validators.required]],
    isAgeRestricted: [false],
    isItsMadeForKids: [false],
    isShowInCarousel: [false],
    isShowInLatestAndTrending: [false],
  });
  submitted: boolean = false;

  selectAllVisibilityInclude:string = "Select All";
  public uploadingTypes = uploadingTypes;

  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public activateRoute: ActivatedRoute,
    public redirectservice: RedirectService) {
    this.activateRoute.params.subscribe(params => {
      this.videoId = params.videoid;
      this.getRequiredData();
    })


  }

  ngOnInit(): void {
  }

  getRequiredData() {
    combineLatest([
      this.commonservice.getMethod(APIS.MASTER_GET_ALL_GENRES),
      this.commonservice.getMethod(APIS.MASTER_GET_ALL_COUNTRIES),
      this.commonservice.getMethod(APIS.ADMIN_GET_VIDEO_DETAILS(this.videoId))
    ]).subscribe(([genresRes, countriesRes, video]) => {
      console.log("video==>", video);
      if (countriesRes.statusCode == 200) {
        this.countries = countriesRes.data;
        this.visibility = countriesRes.data;
      } else {
        this.commonservice.snackbar(countriesRes.message, 'error')
      }

      if (genresRes.statusCode == 200) {
        this.backupGenres = genresRes.data;
        this.genres = genresRes.data;
      } else {
        this.commonservice.snackbar(genresRes.message, 'error')
      }

      if (video.statusCode == 200) {
        this.video = video.data;
        this.videoForm.patchValue({
          countryOrigin: this.video?.countryOrigin,
          contentCertificate: this.video?.contentCertificate,
          rating: this.video?.rating,
          movieLength: this.video?.movieLength,
          visibility: {
            exclude: this.video?.visibility?.exclude[0] !== "" ? this.video?.visibility?.exclude : [],
            include: this.video?.visibility?.include[0] !== "" ? this.video?.visibility?.include : [],
          },
          genres: this.video?.genres.length !== 0 ? this.video?.genres : [],
          isAgeRestricted: this.video?.isAgeRestricted,
          isItsMadeForKids: this.video?.isItsMadeForKids,
          isShowInCarousel: this.video?.isShowInCarousel,
          isLatestAndTrending: this.video?.isLatestAndTrending,
        });

        this.refreshVisability();
        this.refreshGenres();
        console.log("videoForm", this.videoForm)
      } else {
        this.commonservice.snackbar(video.message, 'error')
      }

    })
  }

  changeSelection(event: any, field: string) {
    if(this.selectAllVisibilityInclude === event.target.value){
      this.videoForm.patchValue({
        visibility: {
          [field]: [...this.countries.map((x: any) => x.name)],
        }
      });
    }else{
      this.videoForm.patchValue({
      visibility: {
        [field]: [...this.videoForm.value.visibility[field], event.target.value],
      }
    });
    }

    
    this.refreshVisability();
  }


  removeVisability(name: string) {
    this.videoForm.patchValue({
      visibility: {
        ['include']: [...this.videoForm.value.visibility['include'].filter((x: string) => x != name)]
      }
    });
    this.videoForm.patchValue({
      visibility: {
        ['exclude']: [...this.videoForm.value.visibility['exclude'].filter((x: string) => x != name)]
      }
    });
    this.refreshVisability();
  }

  refreshVisability() {
    let selectedVisabilities = [...this.videoForm.value.visibility['exclude'], ...this.videoForm.value.visibility['include']]
    let remainingVisability = this.countries.filter((x: any) =>
      !selectedVisabilities.includes(x.name)
    );
    this.visibility = remainingVisability;
  }

  changeGenreSelection(event: any) {
    this.videoForm.patchValue({
      genres: [...this.videoForm.value.genres, event.target.value],
    });
    this.refreshGenres();
  }

  removeGenre(name: string) {
    this.videoForm.patchValue({
      genres: [...this.videoForm.value.genres.filter((x: string) => x != name)],
    });
    this.refreshGenres();
  }

  refreshGenres() {
    let remainingGenres = this.backupGenres.filter((x: any) =>
      !this.videoForm.value.genres.includes(x.genreName)
    );
    this.genres = remainingGenres;
  }


  updateVideoDetails() {
    this.videoForm.patchValue({
      id: this.videoId
    })
    console.log("videoForm", this.videoForm);
    this.submitted = true;
    if (!this.videoForm.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }

    var url = APIS.ADMIN_VIDEO_SAVE_OR_UPDATE;
    let video: any = this.videoForm.getRawValue();
    this.commonservice.postMethod(url, video).subscribe((res: any) => {
      console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
      if (res.statusCode == 200) {
        // this.videoForm.reset();
        // this.videoForm.patchValue({
        //   visibility: {
        //     exclude: [],
        //     include: [],
        //   },
        //   genres: [],
        // });
        // this.refreshVisability();
        // this.refreshGenres();
        this.commonservice.snackbar("Video Updated Successfully");
        this.redirectservice.uploadVideos(this.videoId);
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

  fileUploaded(event:any) {
    console.log("event==>", event)
  }

}
