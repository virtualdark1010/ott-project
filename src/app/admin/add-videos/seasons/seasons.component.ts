import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

@Component({
  selector: 'app-seasons',
  templateUrl: './seasons.component.html',
  styleUrls: ['./seasons.component.scss']
})
export class SeasonsComponent implements OnInit {
  videoId: any;
  seasonId:any;

  season:any;

  seasonForm = this.fb.group({
    seasonId: [null],
    videoId: [null],
    title: ['', [Validators.required]],
    subTitle: ['', [Validators.required]],
    releaseDate: [null, [Validators.required]],
    description: ['', [Validators.required]],

    movieLength: ['', [Validators.required]],
    seasonNumber: [null, [Validators.required]],
  })

  isEditMode: boolean = false;
  submitted: boolean = false;
  constructor(public fb: FormBuilder,
    public commonservice: CommonService,
    public videocontrolservice: VideoControlsService,
    public redirectservice: RedirectService,
    public activateRoute: ActivatedRoute,
    public router: Router) {

    activateRoute.params.subscribe(param => {
      if (param.videoid) {
        this.videoId = param.videoid;
        this.seasonId = param?.seasonid || '';

        if(this.seasonId){
          this.isEditMode = true;
          this.getSeasonDetails();
        }
      } else {
        this.isEditMode = false;
      }
    })

  }

  ngOnInit(): void {

  }


  getSeasonDetails() {
    this.commonservice.getMethod(APIS.GET_SEASION_DETAILS(this.seasonId))
      .subscribe((res: any) => {
        console.log("GET_SEASION_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.season = res.data;
          this.seasonForm.patchValue(this.season);
          console.log("this.videoForm", this.seasonForm);
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }

  onDateSelect(event: any) {
    let releasedate = this.videocontrolservice.getStringFormatDate(event);
    this.seasonForm.patchValue({ releaseDate: releasedate });
  }

  cancel(){
    this.router.navigate([`/admin/videos`]);
  }

  episodes(){
    this.redirectservice.episodes(this.seasonId);
  }

  createSeason() {
    // activeModal.dismiss('Cross click');

    // console.log("videoForm", this.seasonForm);

    // return
    this.seasonForm.controls.videoId.setValue(this.videoId);
    if(this.seasonId){
      this.seasonForm.controls.seasonId.setValue(this.seasonId);
    }

    this.submitted = true;

    if (!this.seasonForm.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }
    var url = APIS.ADMIN_SEASON_SAVE_OR_UPDATE;
    let season = this.seasonForm.getRawValue();
    debugger
    this.commonservice.postMethod(url, season)
      .subscribe((res: any) => {
        console.log("ADMIN_SEASON_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.seasonForm.reset();
          this.commonservice.snackbar(this.isEditMode ? "Season Updated Successfully" : "Season Created Successfully");
          // this.redirectservice.privacyAndRestriction(res.data.id);
          if(this.isEditMode){
          this.redirectservice.createNewSeason(this.videoId);
          }
          this.submitted = false;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })

  }


}
