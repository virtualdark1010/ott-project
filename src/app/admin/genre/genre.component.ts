import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.scss']
})
export class GenreComponent implements OnInit {
  public genreForm = new FormGroup({
    id: new FormControl(''),
    genre: new FormControl("", [Validators.required]),
  });
  submitted: boolean = false;
  genre: any = [];
  genreHeaders = [
    { field: 'genreName', lable: 'Genre', type: 'text' }
  ];

  editMode: boolean = false

  constructor(public commonservice: CommonService) { }

  ngOnInit(): void {
    this.getGenre();
    console.log("value" + this.genreHeaders);
  }


  getGenre() {
    var url = APIS.ADMIN_GENRE;
    this.commonservice.getMethod(url)
      .subscribe((res: any) => {
        console.log("ADMIN_Genre==>", res);
        if (res.statusCode == 200) {
          this.genre = res.data;
          console.log("result" + this.genre);
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  cancle() {
    this.genreForm.reset();
    this.editMode = false;
  }

  createNewGenre() {
    this.submitted = true;
    if (this.genreForm.invalid) {
      this.commonservice.snackbar("Please fill Genre", "error");
      return;
    }
    let reqData: any = this.genreForm.getRawValue();
    this.saveOrUpdate(reqData);
  }

  saveOrUpdate(reqData: any) {
    this.commonservice.postMethod(APIS.ADMIN_GENRE_SAVE_OR_UPDATE, reqData)
      .subscribe((res: any) => {
        console.log("ADMIN_Genre_SaveORUpdate==>", res);
        if (res.statusCode == 200) {
          this.submitted = false;
          this.genreForm.reset();
          this.commonservice.snackbar(this.editMode ? "Genre Updated Successfully" : "Genre Created Successfully");
          this.getGenre();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  delete(id:any){
    var deleteUrl = APIS.ADMIN_GENRE_DELETE;
    this.commonservice.postMethod(deleteUrl, id)
    .subscribe((res: any) => {
      console.log("ADMIN_Genre_Delete==>", res);
      if (res.statusCode == 200) {
        this.submitted = false;
        this.genreForm.reset();
        this.commonservice.snackbar("Genre Deleted Successfully");
        this.getGenre();
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

  onSelectItemForEdit(item: any) {
    console.log("item==>", item);
    window.scroll(0, 0);
    this.editMode = true;
    this.genreForm.patchValue({
      id: item.id,
      genre: item.genreName
    });
  }

  onSelectItemForDelete(item: any) {
    console.log("item==>", item);
     this.delete(item.id);
  }

}
