import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';

@Component({
  selector: 'app-upload-video-modal',
  templateUrl: './upload-video-modal.component.html',
  styleUrls: ['./upload-video-modal.component.scss']
})
export class UploadVideoModalComponent implements OnInit {
  uploadformFile: boolean = true;
  model: NgbDateStruct | undefined;

  types:any = [];
  visibility: any = [];
  genres: any = [];
  languages:any = [];

  videoForm = this.fb.group({
    type: [''],
    title: [''],
    subTitle: [''],
    season: [null],
    episode: [null],
    releaseDate: [null],
    description: [''],
    isAgeRestricted: [false],
    rating: [''],
    movieLength: [''],
    visibility: [['']],
    genres: [['']],
    castId: [['']],
    countryOrigin: [''],
    language: [''],
  })



  constructor(public activeModal: NgbActiveModal, public fb: FormBuilder,
    public commonservice: CommonService, public videocontrolservice: VideoControlsService) {
  }

  ngOnInit(): void {
  }


  openFormFeilds() {
    this.uploadformFile = !this.uploadformFile
  }

  onDateSelect(event: any) {
    let releasedate = this.videocontrolservice.getStringFormatDate(event);
    this.videoForm.patchValue({ releaseDate: releasedate });
  }

  saveVideo() {
    // activeModal.dismiss('Cross click')
    console.log("videoForm", this.videoForm);

    if (!this.videoForm.valid) {
      this.commonservice.snackbar("Form Invalid", 'error');
      return;
    }

    var url = APIS.ADMIN_VIDEO_SAVE_OR_UPDATE;
    let user = this.videoForm.getRawValue();
    this.commonservice.postMethod(url, user)
      .subscribe((res: any) => {
        console.log("ADMIN_VIDEO_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.videoForm.reset();
          this.commonservice.snackbar("Video Created Successfully");
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })

  }

  publishVideo() {
    // activeModal.dismiss('Cross click')
    console.log("videoForm", this.videoForm)
  }

}
