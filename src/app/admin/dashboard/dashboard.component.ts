import { Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
import { reduceEachLeadingCommentRange } from 'typescript';
Chart.register(...registerables);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  active = 1;
  recentActive = 1;
  constructor() { }
  items =[
    {icon:'fa-video', imgIcon:'',count:'21234',label:'Total Contents'},
    {icon:'fa-comment-alt-dots', imgIcon:'',count:'121234',label:'This comments'},
    {icon:'fa-user-friends', imgIcon:'',count:'234234 ',label:'Total Accounts'},
    {icon:'', imgIcon:'./assets/imgs/logo-icon.png',count:'4234',label:'Kismet Specials'},
  ];

  tableData =[
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
  ]
  tableItems=[
    {content:'Gul Makai',type:'Action, Drama',date:'Dec 18, 2021',progress:'45%',likes:'482', views:'6543'},
    {content:'Happy Bhag Jayegi',type:'Action, Drama',date:'Dec 16, 2021',progress:'45%',likes:'4822', views:'34344'},
    {content:'Sarbjit',type:'Action, Romantic ',date:'Dec 06, 2021',progress:'35%',likes:'4182', views:'44332'},
    {content:'Teefa in Trouble',type:'Action, Suspense ',date:'Dec 02, 2021',progress:'56%',likes:'2482', views:'3223'},
    {content:'The Wedding Guest',type:'Action, Suspense ',date:'Nov 30, 2021',progress:'85%',likes:'2312', views:'53433'},
    {content:'Kurban',type:'Action, Drama',date:'Nov 26, 2021',progress:'44%',likes:'2334', views:'34432'},
    {content:'Katilana Manjil',type:'Action, Romantic ',date:'Nov 22, 2021',progress:'76%',likes:'4233', views:'534232'},
  ]



  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      y: {
        grid: {
          color: 'white'
        }
      },
      x: {
        
        barPercentage: 0.1,
        grid: {
          color: 'white'
        }
      }
    }
  };
  public barChartLabels = ['Movies1', 'Movies2', 'Movies3', 'Movies4', 'Movies5', 'Movies6', 'Movies7','Movies8', 'Movies9', 'Movies10'];
  public barChartType = 'bar';
  public barChartLegend = false;

  public barChartData = [
    {
      barPercentage: .5,
      barThickness: 16,
      maxBarThickness: 20,
      backgroundColor: '#E9C662',
      minBarLength: 12, 
      data: [48, 38, 65, 39, 66, 17, 80,16,100,43], 
      // label: 'Series B'
    }
  ];

  ngOnInit(): void {
  }

}
