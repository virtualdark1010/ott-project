import { Component, OnInit } from '@angular/core';
import { Chart, registerables } from 'chart.js';
import { reduceEachLeadingCommentRange } from 'typescript';
Chart.register(...registerables);

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  active = 1;
  recentActive = 1;
  constructor() { }
  items =[
    {icon:'fa-video', imgIcon:'',count:'21234',label:'Total Contents'},
    {icon:'fa-comment-alt-dots', imgIcon:'',count:'121234',label:'This comments'},
    {icon:'fa-user-friends', imgIcon:'',count:'234234 ',label:'Total Accounts'},
    {icon:'', imgIcon:'./assets/imgs/logo-icon.png',count:'4234',label:'Kismet Specials'},
  ];

  tableData =[
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-1.png',userName:'Amira',userLocation:'Lahore',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-2.png',userName:'Afaf',userLocation:'Karachi',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
    {userImg:'./assets/imgs/avatar-3.png',userName:'Aamna',userLocation:'Multan',comment:'It’s not just a movie  it’s a emotions everybody feel it trust me you got a goosebumps with every scene', date:'Feb 18, 2022'},
  ]
  tableItems=[
    {content:'RRR',type:'Action, Drama',date:'Dec 18, 2021',progress:'45%',likes:'10', views:'16'},
    {content:'Raja Rani',type:'Action, Drama',date:'Dec 16, 2021',progress:'45%',likes:'13', views:'20'},
    {content:'Saaho',type:'Action, Romantic ',date:'Dec 06, 2021',progress:'35%',likes:'16', views:'24'},
    {content:'Aranyak',type:'Action, Suspense ',date:'Dec 02, 2021',progress:'56%',likes:'6', views:'11'},
    {content:'Radhe Shyam',type:'Action, Suspense ',date:'Nov 30, 2021',progress:'85%',likes:'9', views:'17'},
    {content:'NSK',type:'Action, Drama',date:'Nov 26, 2021',progress:'44%',likes:'23', views:'26'},
    {content:'Best Seller',type:'Action, Romantic ',date:'Nov 22, 2021',progress:'76%',likes:'20', views:'53'},
  ]



  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      y: {
        grid: {
          color: 'white'
        }
      },
      x: {
        
        barPercentage: 0.1,
        grid: {
          color: 'white'
        }
      }
    }
  };
  public barChartLabels = ['Movies1', 'Movies2', 'Movies3', 'Movies4', 'Movies5', 'Movies6', 'Movies7','Movies8', 'Movies9', 'Movies10'];
  public barChartType = 'bar';
  public barChartLegend = false;

  public barChartData = [
    {
      barPercentage: .5,
      barThickness: 16,
      maxBarThickness: 20,
      backgroundColor: '#E9C662',
      minBarLength: 12, 
      data: [48, 38, 65, 39, 66, 17, 80,16,100,43], 
      // label: 'Series B'
    }
  ];

  ngOnInit(): void {
  }

}
