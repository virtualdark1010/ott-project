import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AdminService } from 'src/app/providers/admin.service';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { UploadVideoModalComponent } from '../upload-video-modal/upload-video-modal.component';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.scss']
})
export class AdminNavbarComponent implements OnInit {

  toggleMenu:boolean = true;

  constructor(private modalService: NgbModal, 
    public adminservice: AdminService, 
    public router: Router,
    public redirectservice: RedirectService,
    public commonservice : CommonService ) { }

  toggleButton(){ 
    this.toggleMenu = !this.toggleMenu
    this.adminservice.toggleMenu$.next('toggle')
  }

  openUploadVideoModal() {

    this.redirectservice.createVideo();

    // this.router.navigate(['/admin/upload'])

    // this.modalService.open(UploadVideoModalComponent, {
    //   size: 'xxl',
    //   centered:true,
    //   windowClass:'modal-default'
    // });
  }

  ngOnInit(): void {
  }


  logout(){
    this.commonservice.logout();
    this.router.navigate(['/admin/login']);
  }

}
