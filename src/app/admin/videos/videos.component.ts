import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { DeleteModalComponent } from 'src/app/shared/delete-modal/delete-modal.component';
import { uploadingTypes } from '../../shared/video-statics';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {

  videos: any = []
  // videosByGenre!: Observable<any>;

  showAddVideo: boolean = false;

  videoActors: any;
  selectedFiles: Array<File> = [];

  uploading: string = "";
  public uploadingTypes = uploadingTypes;
  existedSeasons: any = [];
  constructor(public videocontrolservice: VideoControlsService,
    public commonservice: CommonService, public http: HttpClient,
    public redirectservice: RedirectService,
    private modalService: NgbModal) { }

  ngOnInit(): void {
    // this.videocontrolservice.getVideosByGenre('action').then(res => this.videosByGenre = res);
    this.getAllVideos()
  }

  getAllVideos() {
    var url = APIS.ADMIN_GET_ALL_VIDEOS;
    this.commonservice.getMethod(url)
      .subscribe((res: any) => {
        console.log("ADMIN_GET_ALL_VIDEOS==>", res);
        if (res.statusCode == 200) {
          this.videos = res.data.map((x: any) => { return { ...x, 'showmore': false } });

          console.log("this.videos", this.videos)
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  getSeasons(event: any, item: any) {
    if (event) {
      console.log(event, "item", item);
      this.commonservice.getMethod(APIS.GET_SEASONS_BY_VIDEOID(item.id))
        .subscribe((res: any) => {
          console.log("GET_SEASONS_BY_VIDEOID==>", res);
          if (res.statusCode == 200) {
            this.existedSeasons = res.data;
          } else {
            this.commonservice.snackbar(res.message, 'error')
          }
        })
    }
  }


  showMore(item: any) {
    this.videos.forEach((x: any) => { x.showmore = false })
    item.showmore = true;
    console.log("item", item)

    this.getVideoDetails(item.id);
  }

  getVideoDetails(videoId: any) {
    this.commonservice.getMethod(APIS.ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID(videoId))
      .subscribe((res: any) => {
        console.log("ADMIN_GET_CAST_DETAIL_LIST_BY_VIDEO_ID==>", res);
        if (res.statusCode == 200) {
          this.videoActors = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      })
  }

  updateVideo(item: any) {
    this.redirectservice.updateVideo(item.id);
  }

  castAndCrew(item: any) {
    this.redirectservice.castAndCrew(item.id);
  }

  privacy(item: any) {
    this.redirectservice.privacyAndRestriction(item.id);
  }

  uploadingVidoes(item: any) {
    this.redirectservice.uploadVideos(item.id);
  }

  newSeason(item: any) {
    this.redirectservice.createNewSeason(item.id);
  }

  selectSeason(season: any) {
    this.redirectservice.editSeason(season.videoId, season.seasonId);
  }



  deleteVideo(item: any) {
    this.modalService.open(DeleteModalComponent, {
      centered: true, backdrop: 'static', keyboard: false
    }).dismissed.subscribe((confirmed: boolean) => {
      if (confirmed) {
        var url = APIS.ADMIN_DELETE_VIDEO_BY_ID(item.id);
        this.commonservice.getMethod(url)
          .subscribe((res: any) => {
            console.log("ADMIN_DELETE_VIDEO_BY_ID==>", res);
            if (res.statusCode == 200) {
              this.commonservice.snackbar(res.message, 'success')
              this.getAllVideos();
            } else {
              this.commonservice.snackbar(res.message, 'error')
            }
          }, err => {
            console.log("err==>", err);
            this.commonservice.snackbar(err.message, 'error')
          })
      }
    })
  }

}
