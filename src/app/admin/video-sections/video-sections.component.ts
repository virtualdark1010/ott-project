import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RedirectService } from 'src/app/providers/redirect.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';
import { combineLatest } from 'rxjs';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-video-sections',
  templateUrl: './video-sections.component.html',
  styleUrls: ['./video-sections.component.scss']
})
export class VideoSectionsComponent implements OnInit {
  genres: any = [];
  submitted: boolean = false;
  backupGenres: any = [];
  videoType: any;

  videoSectionsForm = this.getSectionForm();

  getSectionForm() {
    return this.formbuilder.group({
      id: [null],
      videoType: ['', [Validators.required]],
      sectionName: new FormControl("", [Validators.required]),
      genres: [[], [Validators.required]],
      sequence: [0, [Validators.required]],
    })
  }


  videoSectionHeaders = [
    { field: 'sequence', lable: 'Sequence', type: 'text' },
    { field: 'sectionName', lable: 'Section Name', type: 'text' },
    { field: 'videoType', lable: 'Video Type', type: 'text' },
    { field: 'genres', lable: 'Genres', type: 'arraypills' }
  ];
  editMode: boolean = false
  sections: any = [];

  constructor(public formbuilder: FormBuilder,
    public commonservice: CommonService,
    public videocontrolservice: VideoControlsService,
    public redirectservice: RedirectService) {
  }

  ngOnInit(): void {
    this.getVideoSectionDetails();
    this.getSections();
  }

  getVideoSectionDetails() {
    combineLatest([
      this.commonservice.getMethod(APIS.MASTER_GET_ALL_GENRES),
      this.commonservice.getMethod(APIS.MASTER_GET_VIDEO_TYPES)
    ]).subscribe(([genresRes, videores]) => {

      if (genresRes.statusCode == 200) {
        this.backupGenres = genresRes.data;
        this.genres = genresRes.data;
      } else {
        this.commonservice.snackbar(genresRes.message, 'error');
      }
      if (videores.statusCode == 200) {
        this.videoType = videores.data;
        this.videoSectionsForm.patchValue({
          videoType: this.videoSectionsForm.value.videoType,
        });
        console.log("data" + this.videoType);
      } else {
        this.commonservice.snackbar(genresRes.message, 'error');
      }
    })
  }

  getSections() {
    var url = APIS.ADMIN_GET_ALL_SECTIONS;
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("ADMIN_GET_ALL_SECTIONS==>", res);
      if (res.statusCode == 200) {
        this.sections = res.data;
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

  changeGenreSelection(event: any) {
    this.videoSectionsForm.patchValue({
      genres: [...this.videoSectionsForm.value.genres, event.target.value],
    });
    this.refreshGenres();
  }

  removeGenre(name: string) {
    this.videoSectionsForm.patchValue({
      genres: [...this.videoSectionsForm.value.genres.filter((x: string) => x != name)],
    });
    this.refreshGenres();
  }

  refreshGenres() {
    let remainingGenres = this.backupGenres.filter((x: any) =>
      !this.videoSectionsForm.value.genres.includes(x.genreName)
    );
    this.genres = remainingGenres;
  }

  cancle() {
    this.videoSectionsForm = this.getSectionForm();
    this.editMode = false;
  }

  createNewSection() {
    this.submitted = true;
    if (this.videoSectionsForm.invalid) {
      this.commonservice.snackbar("Form Invalid", "error");
      return;
    }
    let reqData: any = this.videoSectionsForm.getRawValue();
    this.saveOrUpdate(reqData);
  }

  saveOrUpdate(reqData: any) {
    this.commonservice.postMethod(APIS.ADMIN_SECTION_SAVE_OR_UPDATE, reqData)
      .subscribe((res: any) => {
        console.log("ADMIN_Genre_SaveORUpdate==>", res);
        if (res.statusCode == 200) {
          this.submitted = false;
          this.videoSectionsForm = this.getSectionForm();
          this.commonservice.snackbar(this.editMode ? "Section Updated Successfully" : "Section Created Successfully");
          this.getSections();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  onSelectItemForEdit(item: any) {
    this.editMode = true;
    debugger
    this.videoSectionsForm.patchValue({
      id: item.id,
      sectionName: item.sectionName,
      genres: item.genres,
      videoType: item.videoType,
      sequence: item.sequence
    });
    this.refreshGenres();
  }

  onSelectItemForDelete(item: any) {
    var deleteUrl = APIS.ADMIN_SECTION_DELETE_BY_ID(item.id);
    this.commonservice.postMethod(deleteUrl, {})
      .subscribe((res: any) => {
        console.log("ADMIN_SECTION_DELETE_BY_ID==>", res);
        if (res.statusCode == 200) {
          this.commonservice.snackbar("Section Deleted Successfully");
          this.getSections();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

}
