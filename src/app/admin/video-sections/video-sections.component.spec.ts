import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoSectionsComponent } from './video-sections.component';

describe('VideoSectionsComponent', () => {
  let component: VideoSectionsComponent;
  let fixture: ComponentFixture<VideoSectionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoSectionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoSectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
