import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthGuard } from '../providers/guards/auth.guard';
import { ActorsComponent } from './actors/actors.component';
import { GenreComponent } from './genre/genre.component';
import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { VideosComponent } from './videos/videos.component';

import { UploadVideoComponent } from './add-videos/upload-video/upload-video.component';
import { CastAndCrewComponent } from './add-videos/cast-and-crew/cast-and-crew.component';
import { CreateVideoComponent } from './add-videos/create-video/create-video.component';
import { PrivacyVideoComponent } from './add-videos/privacy-video/privacy-video.component';
import { VideoSectionsComponent } from './video-sections/video-sections.component';
import { LanguagesComponent } from './languages/languages.component';
import { VideoTypesComponent } from './videotypes/videotypes.component';
import { PaymentComponent } from './add-videos/payment/payment.component';
import { MonetisationComponent } from './monetisation/monetisation.component';
import { SubscriptionComponent } from './monetisation/subscription/subscription.component';
import { OrdersComponent } from './monetisation/orders/orders.component';
import { ContractsComponent } from './monetisation/contracts/contracts.component';
import { TransactionComponent } from './monetisation/transaction/transaction.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { SeasonsComponent } from './add-videos/seasons/seasons.component';
import { EpisodesComponent } from './episodes/episodes.component';
import { AddEpisodesComponent } from './add-videos/add-episodes/add-episodes.component';
import { AddAdminComponent } from './add-admin/add-admin.component';


const routes: Routes = [
  { path: 'login', component: LoginAdminComponent },
  {
    path: "", component: AdminComponent,
    canActivateChild: [AdminAuthGuard],
    children: [
      {
        path: "", component: DashboardComponent
      },
      {
        path: "video/create", component: CreateVideoComponent
      },
      {
        path: "video/update/:videoid", component: CreateVideoComponent
      },
      {
        path: "video/privacy/:videoid", component: PrivacyVideoComponent
      },
      {
        path: "video/cast-crew/:videoid", component: CastAndCrewComponent
      },
      {
        path: "video/upload/:videoid", component: UploadVideoComponent
      },
      {
        path: "video/seasons/:videoid", component: SeasonsComponent
      },
      {
        path: "video/seasons/:videoid/:seasonid", component: SeasonsComponent
      },
      {
        path: "video/episodes/:seasonid", component: EpisodesComponent
      },
      {
        path: "video/episodes/:seasonid/:episodeid", component: EpisodesComponent
      },
      // {
      //   path: "video/add-episode/:seasonid", component: AddEpisodesComponent
      // },
      // {
      //   path: "video/add-episode/:seasonid/:episodeid", component: AddEpisodesComponent
      // },
      {
        path: "videos", component: VideosComponent
      },
      {
        path: "actors", component: ActorsComponent
      },
      {
        path: "genres", component: GenreComponent
      },
      {
        path: "video/sections", component: VideoSectionsComponent
      },
      {
        path: "languages", component: LanguagesComponent
      },
      {
        path: "video/types", component: VideoTypesComponent
      },
      {
        path: "video/payment/:videoid", component: PaymentComponent
      },
      {
        path: "monetisation/subscription", component: SubscriptionComponent
      },
      {
        path: "monetisation/orders", component: OrdersComponent
      },
      {
        path: "monetisation/contracts", component: ContractsComponent
      },
      {
        path: "monetisation/transaction", component: TransactionComponent
      },
      {
        path: "analytics", component: AnalyticsComponent
      },
      {
        path: "add-admin", component: AddAdminComponent
      }
      // {
      //   path:"type-cast", component:TypeCastComponent
      // }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
