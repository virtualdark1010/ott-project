import { Component, OnInit } from '@angular/core';
import { AdminService } from '../providers/admin.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  sidemenuToggled: boolean = false;

  constructor(public adminservice: AdminService) {
    adminservice.toggleMenu$.subscribe(data => {
      if (data === 'toggle') {
        this.toggleMenu();
      }
    })
  }
  ngOnInit(): void {
  }

  toggleMenu() {
    this.sidemenuToggled = !this.sidemenuToggled;
  }




}
