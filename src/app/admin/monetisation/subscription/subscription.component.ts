import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';


@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss']
})
export class SubscriptionComponent implements OnInit {
  duration: string = '';
  url: string = '';
  showValues: boolean = false;
  constructor(public videocontrolservice: VideoControlsService,
    public commonservice: CommonService,
    public http: HttpClient,
  ) { }
  // private datePipe: DatePipe) { }

  ngOnInit(): void {

  }
  onSelect(){
    this.showValues = true;
    if(this.duration == "30 Days")
    this.url = APIS.ONEMONTH;
    if(this.duration == "Three Months")
    this.url = APIS.THREEMONTHS;
    if(this.duration == "Six Months")
    this.url = APIS.SIXMONTHS;
    if(this.duration == "One Year")
    this.url = APIS.ONEYEAR;
    if(this.duration == "ALL Details")
    this.url = APIS.ALL;
    
    this.commonservice.getMethod(this.url).subscribe((res: any) => {
    console.log(res);
    });

    
  }


}
