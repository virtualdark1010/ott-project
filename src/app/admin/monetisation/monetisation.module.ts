import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { SubscriptionComponent } from './subscription/subscription.component';
import { OrdersComponent } from './orders/orders.component';
import { ContractsComponent } from './contracts/contracts.component';
import { TransactionComponent } from './transaction/transaction.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
SubscriptionComponent,
OrdersComponent,
ContractsComponent,
TransactionComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class MonetisationModule { }
