import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonetisationComponent } from './monetisation.component';

describe('MonetisationComponent', () => {
  let component: MonetisationComponent;
  let fixture: ComponentFixture<MonetisationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonetisationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonetisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
