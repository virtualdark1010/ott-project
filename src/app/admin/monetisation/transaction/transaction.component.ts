import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';


@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {
  showValues: boolean = false;
  constructor(public videocontrolservice: VideoControlsService,
    public commonservice: CommonService,
    public http: HttpClient,
  ) { }
  // private datePipe: DatePipe) { }

  ngOnInit(): void {

  }
  onSelect(){
    this.showValues = true;
  }

}
