import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';


@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  constructor(public videocontrolservice: VideoControlsService,
    public commonservice: CommonService,
    public http: HttpClient,
  ) { }
  // private datePipe: DatePipe) { }

  ngOnInit(): void {

  }


}
