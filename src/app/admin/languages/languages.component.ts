import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit {
  public languageForm = new FormGroup({
    id: new FormControl(''),
    lname: new FormControl("", [Validators.required]),
  });

  submitted: boolean = false;

  languages: any = [];
  languageHeaders = [
    { field: '_id', lable: 'S.No', type: 'text' },
    { field: 'name', lable: 'Language', type: 'text' }
  ];

  constructor(public commonservice: CommonService) { }


  ngOnInit(): void {
    this.getlanguages();
  }

  getlanguages() {
    var url = APIS.MASTER_GET_ALL_LANGUAGES;
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("ADMIN_GET_ALL_LANGUAGES==>", res);
      if (res.statusCode == 200) {
        this.languages = res.data;
        console.log("result" + this.languages);
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

  cancle() {
    this.languageForm.reset();
    this.submitted = false;
  }

  createNewLanguage() {
    this.submitted = true;
    if (this.languageForm.invalid) {
      this.commonservice.snackbar("Please fill Language", "error");
      return;
    }
    let reqData: any = this.languageForm.getRawValue();
    this.saveOrUpdate(reqData);
  }

  saveOrUpdate(reqData: any) {
    this.commonservice.postMethod(APIS.ADMIN_LANGUAGE_SAVE_OR_UPDATE, reqData)
      .subscribe((res: any) => {
        console.log("ADMIN_LANGUAGE_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.submitted = false;
          this.languageForm.reset();
          this.commonservice.snackbar("Language Created Successfully");
          this.getlanguages();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  onSelectItemForDelete(_id: any) {
    console.log("item==>", _id);
    var deleteUrl = APIS.ADMIN_DELETE_LANGUAGE;
    this.commonservice.postMethod(deleteUrl, _id)
      .subscribe((res: any) => {
        console.log("ADMIN_DELETE_LANGUAGE==>", res);
        if (res.statusCode == 200) {
          this.submitted = false;
          this.languageForm.reset();
          this.commonservice.snackbar("Language Deleted Successfully");
          this.getlanguages();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }
}
