import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Data, Router } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { ModalService } from 'src/app/providers/modal.service';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.scss']
})
export class LoginAdminComponent implements OnInit {
  submitted: boolean = false;
  public signInForm!: FormGroup;
  public signInWith!: FormGroup;


  constructor(public modalservice: ModalService,
    public commonservice: CommonService,
    public router: Router) { }

  ngOnInit(): void {
    this.signInForm = new FormGroup({
      username: new FormControl(null, [Validators.required]),
      // email: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
    });
  }
  onSignIn() {
    this.submitted = true;
    if (this.signInForm.valid) {
      let reqObj = this.signInForm.getRawValue();
      this.signInApi(reqObj);
    }
    else {
      this.commonservice.snackbar("Invalid Form", "error");
    }
  }


  signInApi(reqObj: any) {
    var url = APIS.USER_LOGIN;
    this.commonservice.postMethod(url, reqObj)
      .subscribe((res: Data) => {
        console.log("Login", res);
        if (res.statusCode == 200) {
          if(res.data.userdata.roles.length === 1){
            this.commonservice.snackbar('please enter your admin credentials', 'error');
            return;
          }
          this.commonservice.setuserInfo(res.data.userdata);
          this.commonservice.setToken(res.data.accessToken);
          this.commonservice.snackbar('Login Successfully', 'success');
          this.modalservice.closeModal();
          this.router.navigate(['/admin']);
        } else {
          this.commonservice.snackbar(res.message, "error");
        }
      },
        err => {
          this.commonservice.snackbar(err.message, "error");
        })
  }

}
