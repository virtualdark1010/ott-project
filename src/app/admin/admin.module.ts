import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { CommonexportsModule } from '../commonexports/commonexports.module';

import { NgChartsModule } from 'ng2-charts';

import { AdminComponent } from './admin.component';
import { LoginAdminComponent } from './login-admin/login-admin.component';
import { SharedModule } from '../shared/shared.module';

import { AdminNavbarComponent } from './admin-navbar/admin-navbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TypeCastComponent } from './type-cast/type-cast.component';
import { VideosComponent } from './videos/videos.component';
import { AdminSidenavComponent } from './admin-sidenav/admin-sidenav.component';
import { UploadVideoModalComponent } from './upload-video-modal/upload-video-modal.component';
import { AdminService } from '../providers/admin.service';
import { ActorsComponent } from './actors/actors.component';
import { GenreComponent } from './genre/genre.component';

import { UploadVideoComponent } from './add-videos/upload-video/upload-video.component';
import { CastAndCrewComponent } from './add-videos/cast-and-crew/cast-and-crew.component';
import { CreateVideoComponent } from './add-videos/create-video/create-video.component';
import { PrivacyVideoComponent } from './add-videos/privacy-video/privacy-video.component';
import { VideoSectionsComponent } from './video-sections/video-sections.component';
import { LanguagesComponent } from './languages/languages.component';
import { VideoTypesComponent } from './videotypes/videotypes.component';
import { PaymentComponent } from './add-videos/payment/payment.component';
import { MonetisationModule } from './monetisation/monetisation.module';
import { AnalyticsComponent } from './analytics/analytics.component';
import { SeasonsComponent } from './add-videos/seasons/seasons.component';
import { EpisodesComponent } from './episodes/episodes.component';
import { AddEpisodesComponent } from './add-videos/add-episodes/add-episodes.component';
import { AddAdminComponent } from './add-admin/add-admin.component';

@NgModule({
  declarations: [
    AdminComponent,
    LoginAdminComponent,
    AdminNavbarComponent,
    DashboardComponent,
    TypeCastComponent,
    VideosComponent,
    AdminNavbarComponent,
    AdminSidenavComponent,
    DashboardComponent,
    UploadVideoModalComponent,
    ActorsComponent,
    GenreComponent,
    UploadVideoComponent,
    CastAndCrewComponent,
    CreateVideoComponent,
    PrivacyVideoComponent,
    VideoSectionsComponent,
    LanguagesComponent,
    VideoTypesComponent,
    PaymentComponent,
    AnalyticsComponent,
    SeasonsComponent,
    EpisodesComponent,
    AddEpisodesComponent,
    AddAdminComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    CommonexportsModule,
    SharedModule,
    NgChartsModule,
    MonetisationModule
  ],
  providers: [
    AdminService,
    DatePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AdminModule { }
