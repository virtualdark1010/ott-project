import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-sidenav',
  templateUrl: './admin-sidenav.component.html',
  styleUrls: ['./admin-sidenav.component.scss']
})
export class AdminSidenavComponent implements OnInit {

  toggleSwitch: boolean = false;

  constructor() { }
  menus = [
    { link: '/admin', icon: 'fa-house', label: 'Dashboard' },
    { link: '/admin/add-admin', icon: 'fa-user', label: 'Admin' },
    { link: '/admin/video/create', icon: 'fa-folder-upload', label: 'Content Upload' },
    { link: '/admin/videos', icon: 'fa-album-collection', label: 'Playlists' },
    { link: '/admin/actors', icon: 'fa-user-secret', label: 'Cast and Crew' },
    { link: '/admin/genres', icon: 'fa-stream', label: 'Genres' },
    { link: '/admin/video/sections', icon: 'fat fa-th-list', label: 'Content Sections' },
    { link: '/admin/analytics', icon: 'fa-chart-pie', label: 'Analytics' },
    { link: '/admin/monetisation/subscription', icon: 'fa-chart-line', label: 'Monetisation' },
    { link: '/admin/languages', icon: 'fi-local-language-regular', label: 'Languages' },
    { link: '/admin/video/types', icon: 'fa-video', label: 'Content Types' },

    // {link:'/admin/2',icon:'fa-comment-alt-dots', label:'Comments'},
    // {link:'/admin/3',icon:'fa-closed-captioning', label:'Subtitles'},
    // {link:'/admin/4',icon:'fa-user-friends', label:'Accounts'},
    // {link:'/admin/5',icon:'fa-cog', label:'Settings'},
  ]
  monest = [
    { link: '/admin/monetisation/subscription', icon: 'fa-rocket-launch', label: 'Subscription' },
    { link: '/admin/monetisation/orders', icon: 'fa-bags-shopping', label: 'Orders' },
    { link: '/admin/monetisation/contracts', icon: 'fa-frown-open', label: 'Conflicts' },
    { link: '/admin/monetisation/transaction', icon: 'fa-credit-card', label: 'Transaction Based' },
  ]

  ngOnInit(): void {
  }

  monestItem() {
    if (this.toggleSwitch)
      this.toggleSwitch = false;
    else
      this.toggleSwitch = true;
  }
  nonmonestItem() {
    if (this.toggleSwitch)
      this.toggleSwitch = false;
  }
}
