import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-add-admin',
  templateUrl: './add-admin.component.html',
  styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {

  adminForm: FormGroup = this.getAdminForm();
  getAdminForm() {
    return this.formbuilder.group({
      userName: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      role: [['ROLE_ADMIN'], [Validators.required]],
      password: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required]],
    })
  }
  submitted: boolean = false;
  adminHeaders = [
    { field: 'username', lable: 'User Name', type: 'text' },
    { field: 'firstName', lable: 'First Name', type: 'text' },
    { field: 'lastName', lable: 'Last Name', type: 'text' },
    { field: 'email', lable: 'Email', type: 'text' },
    { field: 'mobileNumber', lable: 'Mobile Number', type: 'text' },
  ];
  admins: any = [];

  constructor(public formbuilder: FormBuilder,
    public commonservice: CommonService) {
  }

  ngOnInit(): void {
    this.getAdmins();
  }

  getAdmins() {
    var url = APIS.GET_All_USERS;
    this.commonservice.getMethod(url).subscribe((res: any) => {
      console.log("GET_All_USERS==>", res);
      if (res.statusCode == 200) {
        this.admins = res.data;
      } else {
        this.commonservice.snackbar(res.message, 'error')
      }
    }, err => {
      console.log("err==>", err);
      this.commonservice.snackbar(err.message, 'error')
    })
  }

  cancle() {
    this.adminForm = this.getAdminForm();
  }

  createAdmin() {
    this.submitted = true;
    if (this.adminForm.invalid) {
      this.commonservice.snackbar("Form Invalid", "error");
      return;
    }
    let reqData: any = this.adminForm.getRawValue();
    this.saveOrUpdate(reqData);
  }

  saveOrUpdate(reqData: any) {
    this.commonservice.postMethod(APIS.ADD_ADMIN, reqData)
      .subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.submitted = false;
          this.adminForm = this.getAdminForm();
          this.commonservice.snackbar("Admin Created Successfully");
          this.getAdmins();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  onSelectItemForDelete(item: any) {
    var deleteUrl = APIS.DELETE_USER(item.userId);
    this.commonservice.postMethod(deleteUrl, {})
      .subscribe((res: any) => {
        console.log("ADMIN_SECTION_DELETE_BY_ID==>", res);
        if (res.statusCode == 200) {
          this.commonservice.snackbar("Admin Deleted Successfully");
          this.getAdmins();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

}