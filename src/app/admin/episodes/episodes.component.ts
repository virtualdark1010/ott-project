import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { RedirectService } from 'src/app/providers/redirect.service';
import { uploadingTypes } from 'src/app/shared/video-statics';
import { isTemplateExpression } from 'typescript';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent implements OnInit {
  seasonId: any;
  episodes: any = [];

  tableHeaders = [
    { field: 'title', lable: 'Title', type: 'text' },
    { field: 'subTitle', lable: 'Sub Title', type: 'text' },
    { field: 'releaseDate', lable: 'Release Date', type: 'date' },
    { field: 'upload_content', lable: 'Upload Content', type: 'button' }
  ];

  uploadContent: boolean = false;
  public uploadingTypes = uploadingTypes;

  video: any = {
    title: "",
    src: "",
    img: ""
  }

  selectedItem: any;

  constructor(public commonservice: CommonService,
    public redirectservice: RedirectService,
    public activateRoute: ActivatedRoute,
    public router: Router) {
    activateRoute.params.subscribe(param => {
      if (param.seasonid) {
        this.seasonId = param?.seasonid;
        this.getEpisodeDetails();
      }
    })
  }

  ngOnInit(): void {

  }

  refreshEpisodes() {
    this.getEpisodeDetails();
  }

  getEpisodeDetails() {
    this.commonservice.getMethod(APIS.GET_ALL_EPISODES_BY_SEASION_ID(this.seasonId))
      .subscribe((res: any) => {
        console.log("GET_ALL_EPISODES_BY_SEASION_ID==>", res);
        if (res.statusCode == 200) {
          this.episodes = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }


  onSelectItemForEdit(item: any) {
    this.redirectservice.editEpisode(this.seasonId, item.episodeId);
    this.uploadContent = false;
    window.scrollTo(0, 0);
  }

  selectButton(event: any) {
    console.log(event);
    this.selectedItem = event.item;
    if (event.field == 'upload_content') {
      this.video = {
        title: this.selectedItem.title,
        src: this.selectedItem.episodeUrl,
        img: ""
      }
      this.uploadContent = true;
    }

    window.scrollTo(0, 0);
  }

  fileUploaded(event: any) {
    console.log("file uploaded", event)
    this.uploadContent = false;
    this.getEpisodeDetails();
  }

  closeContenUpload() {
    this.uploadContent = false;
  }

}
