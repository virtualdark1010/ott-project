import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { APIS } from 'src/app/apis';
import { CommonService } from 'src/app/providers/common.service';
import { VideoControlsService } from 'src/app/providers/video-controls.service';


@Component({
  selector: 'app-actors',
  templateUrl: './actors.component.html',
  styleUrls: ['./actors.component.scss']
})
export class ActorsComponent implements OnInit {

  public actorsForm = new FormGroup({
    castId: new FormControl(null),
    castName: new FormControl("", [Validators.required]),
    designation: new FormControl("", [Validators.required]),
  //  dob: new FormControl("2000-01-01"),
    description: new FormControl("", [Validators.required]),
    //isActive: new FormControl(true),
  });
  actorDOB: any = '';

  submitted: boolean = false;
  actorEditMode: boolean = false;
  actors = [];
  prefillData: any = {};
  actorsHeaders = [
    { field: 's_no', lable: 'S.No', type: 'text' },
    { field: 'castName', lable: 'Name', type: 'text' },
    { field: 'designation', lable: 'Designation', type: 'text' },
    // { field: 'dob', lable: 'DOB', type: 'date' },
    { field: 'description', lable: 'Description', type: 'text' },
    { field: 'profileDP', lable: 'Profile DP', type: 'fileupload', accept: 'image/*' },
  ];

  constructor(public videocontrolservice: VideoControlsService,
    public commonservice: CommonService,
    public http: HttpClient,
  ) { }
  // private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.getActors();
  }


  getActors() {
    this.commonservice.getMethod(APIS.ADMIN_GET_ALL_CAST_DETAILS)
      .subscribe((res: any) => {
        console.log("ADMIN_GET_ALL_CAST_DETAILS==>", res);
        if (res.statusCode == 200) {
          this.actors = res.data;
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  // onDateSelect(event: any) {
  //   let dob = this.videocontrolservice.getStringFormatDate(event);
  //   this.actorsForm.patchValue({ dob: dob });
  // }

  createNewActor() {
    console.log("actorsForm", this.actorsForm);
    this.submitted = true;
    if (this.actorsForm.invalid) {
      this.commonservice.snackbar("Please fill all the required fields", "error");
      return;
    }

    let reqData: any = this.actorsForm.getRawValue();
    // reqData['castedIn'] = reqData['castedIn'].split(',').map((item: any) => item.trim());
    this.saveOrUpdate(reqData);
  }

  saveOrUpdate(reqData: any) {

    this.commonservice.postMethod(APIS.ADMIN_CAST_SAVE_OR_UPDATE, reqData)
      .subscribe((res: any) => {
        console.log("ADMIN_CAST_SAVE_OR_UPDATE==>", res);
        if (res.statusCode == 200) {
          this.submitted = false;
          //this.actorDOB = '';
          this.actorsForm.reset();
          this.commonservice.snackbar(this.actorEditMode ? "Actor Updated Successfully" : "Actor Created Successfully");
          this.getActors(); 
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }

  cancel() {
    this.actorsForm.reset();
    this.actorEditMode = false;
  }

  onSelectItemForEdit(item: any) {
    console.log("item==>", item);
    this.actorEditMode = true;
    window.scroll(0, 0);
   // this.actorDOB = item.dob;
    this.actorsForm.patchValue({
      castId: item.castId,
      castName: item.castName,
      designation: item.designation,
      description: item.description,
     // isActive: item.isActive
    });
  }

  onSelectItemForDelete(item: any) {
    this.commonservice.postMethod(APIS.ADMIN_CAST_DELETE(item._id), {})
      .subscribe((res: any) => {
        console.log("ADMIN_CAST_Delete==>", res);
        if (res.statusCode == 200) {
          this.commonservice.snackbar("Actor Deleted Successfully");
          this.getActors();
        } else {
          this.commonservice.snackbar(res.message, 'error')
        }
      }, err => {
        console.log("err==>", err);
        this.commonservice.snackbar(err.message, 'error')
      })
  }


  uploadProfile(event: any) {
    this.uploadFile(event.event.files[0], event.item.castId);
  }

  uploadFile(file: File, castId: string) {
    const formData = new FormData();
    formData.append("multipartFile", file);
    this.http.post(APIS.ADMIN_UPLOAD_ARTIST_DP(castId), formData)
      .subscribe((res: any) => {
        if (res.statusCode == 200) {
          this.getActors();
          this.commonservice.snackbar('File uploaded.', 'success');
        } else {
          this.commonservice.snackbar(res.message, 'error');
        }
      })
  }

}
