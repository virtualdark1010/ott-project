export interface Movie {
    title?: string,
    description?: string,
    img?: string,
    videourl?: string,
    onPlay?: boolean,
}

export interface videosrc {
    name: string,
    src: string,
    img: string
}

export type seasonType = {
    seasonId: string | null,
    title: string | null
}
export type episodeType = {
    title: string | null,
    episodeId: string | null,
    episodeUrl: string | null
}